package omnifit.brain.ceragemui.algorithm

import android.graphics.Color
import android.util.SparseArray
import androidx.annotation.ColorInt
import omnifit.brain.ceragemui.R
import omnifit.sdk.common.algorithm.*
import omnifit.sdk.common.roundToDecimal
import kotlin.math.roundToInt

@ColorInt
val COLOR_BAD: Int = Color.parseColor("#FFFF5F5F")
@ColorInt
val COLOR_NOT_GOOD_LOW: Int = Color.parseColor("#FFFFA800")
@ColorInt
val COLOR_NOT_GOOD_HIGH: Int = Color.parseColor("#FFFFA800")
@ColorInt
val COLOR_GOOD: Int = Color.parseColor("#FFA3C535")
@ColorInt
val COLOR_VERY_GOOD: Int = Color.parseColor("#FF29BFD0")
@ColorInt
val COLOR_EXCELLENT: Int = Color.parseColor("#FF5A98ED")

enum class GradeLabel(
    val start: Int,
    val end: Int,
    val label: String
) {
    BAD(0, 20, "Bad"),
    NOT_GOOD(21, 40, "Not good"),
    GOOD(41, 60, "Good"),
    VERY_GOOD(61, 80, "Very good"),
    EXCELLENT(81, 100, "Excellent")
}

fun checkDeepDuring(
    indicatorValue: Double
): Boolean = when (indicatorValue > -1.0) {
    true -> indicatorValue >= 6.0
    else -> false
}

enum class ItemType {
    BRAIN_CONCENTRATION
}

enum class INDICATOR(val label:String) {
    VERY_LOW("매우 낮음"),
    LOW("낮음"),
    NORMAL("보통"),
    HIGH("높음"),
    VERY_HIGH("매우 높음")
}

internal val INDICATOR_LABEL_CACHE: MutableMap<ItemType, SparseArray<String?>> = mutableMapOf()

fun loadIndicatorLabels() {
    INDICATOR_LABEL_CACHE[ItemType.BRAIN_CONCENTRATION] = SparseArray<String?>().apply {
        put(1, INDICATOR.VERY_LOW.label)
        put(2, INDICATOR.LOW.label)
        put(3, INDICATOR.NORMAL.label)
        put(4, INDICATOR.HIGH.label)
        put(5, INDICATOR.VERY_HIGH.label)
    }
}

fun <I : Indicatable<*>> I.label(): String {
    return when (this) {
        is BrainConcentrationIndicator -> INDICATOR_LABEL_CACHE[ItemType.BRAIN_CONCENTRATION]?.get(value() + 1) ?: ""
        else                           -> ""
    }
}


enum class ServiceCategory {
    NORMAL,
    NATURE,
    BEAT;
}