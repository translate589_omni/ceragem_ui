package omnifit.brain.ceragemui

import android.app.Application
import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import omnifit.brain.ceragemui.algorithm.ServiceCategory
import omnifit.brain.ceragemui.helper.ContentHelper
import omnifit.sdk.audioplayer.AudioPlayer
import omnifit.sdk.omnifitbrain.OmnifitBrain
import org.jetbrains.anko.windowManager
import timber.log.Timber
import java.lang.ref.WeakReference

class CeragemApplication : Application() {

    private var contextReference: WeakReference<Context>? = null

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        Timber.plant(Timber.DebugTree())
    }

    override fun onCreate() {
        super.onCreate()

        contextReference = WeakReference(this)
        contextReference?.get()?.let { context ->
            initialize(context)
        }
    }

    private fun initialize(base: Context?) {
        base?.let { context ->
            requireDensity()
            Font.init(context)
            AudioPlayer.init(context)
            OmnifitBrain.init(context)
        } ?: throw IllegalArgumentException("Not attached base context")
    }

    var desiredDisplayMetrics: DisplayMetrics? = null

    /**
     * densityDpi :
     *
     * ldpi = 0.75 (120dpi)
     * mdpi = 1 (160dpi)
     * hdpi = 1.5 (240dpi)
     * xhdpi = 2.0 (320dpi)
     * xxhdpi = 3.0 (480dpi)
     * xxxhdpi = 4.0 (640dpi)
     */
    private fun requireDensity() {
        resources.displayMetrics.density.let { density ->

            Timber.e("--> <변경전> 화면 밀도[$density], 폰트 스케일[${resources.configuration.fontScale}]")

            when {
                density > 0.75f && density < 1.0f -> 1.0f
                density > 1.0f && density < 1.5f  -> 1.5f
                density > 1.5f && density < 2.0f  -> 2.0f
                density > 2.0f && density < 3.0f  -> 3.0f
                density > 3.0f && density < 4.0f  -> 4.0f
                else                              -> return
            }
        }.let(this::changeDensity)
    }

    private fun changeDensity(desiredDensity: Float) {

        Timber.e("--> <변경후> 화면 밀도[$desiredDensity], 폰트 스케일[${resources.configuration.fontScale}]")

        DisplayMetrics().apply {
            windowManager.defaultDisplay.getMetrics(this)
            density = desiredDensity
            densityDpi = (desiredDensity * 160f).toInt()
            scaledDensity = desiredDensity
            xdpi = desiredDensity * 160f
            ydpi = desiredDensity * 160f
        }.let { metrics ->
            resources.displayMetrics.setTo(metrics)
            desiredDisplayMetrics = metrics
        }
    }

    fun close() {
        contextReference?.clear()
    }

    var currentServiceCategory: ServiceCategory = ServiceCategory.NORMAL
}

@GlideModule
class EduGlideModule : AppGlideModule()