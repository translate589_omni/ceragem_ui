package omnifit.brain.ceragemui.component

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.Font
import omnifit.brain.ceragemui.algorithm.checkDeepDuring
import omnifit.brain.ceragemui.model.Healing
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.sdk.common.json.fromJson
import omnifit.sdk.common.millisToFormatString
import omnifit.sdk.common.secondsToMillis
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onSeekBarChangeListener
import org.jetbrains.anko.sdk27.coroutines.onTouch
import timber.log.Timber


class DeepHealingTimeComponent<in T : Any>(
    val result: Healing
) : AnkoComponent<T> {

    private val focusTime by lazy {
        calcDeepTrainingTimes(result.indicatorValues)
    }

    private val elapsedTime by lazy {
        result.elapsedTime
    }

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        // 집중력 시간 그래프
        cardView {
            id = R.id.screen_inner_module_widget_01_id_root_container
            radius = dip(10.0f).toFloat()
            cardElevation = dip(5.0f).toFloat()
            setCardBackgroundColor(Color.WHITE)

            constraintLayout {
                id = R.id.screen_inner_module_widget_01_id_01

                // 타이틀
                nonSpacingTextView(R.string.deep_screen_010) {
                    id = R.id.screen_inner_module_widget_01_id_02
                    typeface = Font.boldByLanguage()
                    textSize = 17.4f
                    textColorResource = R.color.x_465064
                    letterSpacing = -0.05f
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = PARENT_ID
                    marginStart = dip(17.0f)
                    topToTop = PARENT_ID
                    topMargin = dip(22.5f)
                }

                // 총시간
                nonSpacingTextView(
                        context.getString(
                            R.string.deep_screen_020,
                            focusTime.millisToFormatString("HH").toInt(),
                            focusTime.millisToFormatString("mm").toInt(),
                            focusTime.millisToFormatString("ss").toInt()
                        )
                ) {
                    id = R.id.screen_inner_module_widget_01_id_04
                    textSize = 32.3f
//                    textSize = 20f
                    lines = 1
                    typeface = Font.latoRegular
                    setLineSpacing(32.4f,0f)
                    textColorResource = R.color.x_2e2e2e
                    includeFontPadding = false
                    isVisible = elapsedTime != 0L
                }.lparams(wrapContent, dip(23.8f)) {
                    topToBottom = R.id.screen_inner_module_widget_01_id_02
                    startToStart = PARENT_ID
                    endToEnd = PARENT_ID
                    topMargin = dip(25f)
                }

                constraintLayout {
                    id = R.id.screen_inner_module_widget_01_id_05
                    isVisible = elapsedTime != 0L

                    textView {
                        id = R.id.screen_inner_module_widget_01_id_06
                        backgroundResource = R.drawable.ic_graph_num_bg
                        typeface = Font.latoRegular
                        textSize = 12f
                        textColorResource = R.color.x_ffffff
                        letterSpacing = -0.01f
                        lines = 1
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                        setLineSpacing(5.8f,0f)
                        setPadding(0, 0, 0, dip(4.5f))
                    }.lparams(wrapContent, wrapContent) {
                        topToTop = PARENT_ID
                        startToStart = PARENT_ID
                    }

                    seekBar {
                        id = R.id.screen_inner_module_widget_01_id_07
                        max = 100 + BALLOON_CORRECTION_VALUE
                        progress = 0
                        thumb = context.getDrawable(R.drawable.shape_time_bar_thumb)
                        thumbOffset = dip(7.1f)
                        progressDrawable = context.getDrawable(R.drawable.layer_list_time_bar)
                        setPadding(dip(7.1f), 0, dip(7.1f), 0)
                        onTouch(returnValue = true) { _, _ -> }
                        onSeekBarChangeListener {
                            onProgressChanged { seekBar, _, _ ->
                                seekBar?.let { _seekBar ->
                                    updatePercentagePosition(_seekBar, result)
                                }
                            }
                        }
                    }.lparams(dip(281f + 14.2f), wrapContent) {
                        topMargin = dip(1.0f)
                        topToBottom = R.id.screen_inner_module_widget_01_id_06
                        startToStart = PARENT_ID
                        endToEnd = PARENT_ID
                    }

                    nonSpacingTextView {
                        id = R.id.screen_inner_module_widget_01_id_10
                        textSize = 12f
                        lines = 1
                        includeFontPadding = false
                        gravity = Gravity.END
                        text = Spanner().apply {
                            context.getString(
                                R.string.deep_screen_030,
                                elapsedTime.millisToFormatString("HH").toInt(),
                                elapsedTime.millisToFormatString("mm").toInt(),
                                elapsedTime.millisToFormatString("ss").toInt()
                            ).split('|').let { strings ->
                                if (strings.size > 1) {
                                    val sp = sp(12f)
                                    val font_1 = Font.CustomTypefaceSpan(Font.mediumByLanguage())
                                    val font_2 = Font.CustomTypefaceSpan(Font.latoRegular)
                                    val color_1 = ResourcesCompat.getColor(resources, R.color.x_afafaf, null)
                                    val color_2= ResourcesCompat.getColor(resources, R.color.x_767676, null)
                                    append(strings[0], Spans.sizeSP(sp), Spans.custom(font_1), Spans.foreground(color_1))
                                    append(strings[1], Spans.sizeSP(sp), Spans.custom(font_2), Spans.foreground(color_2))
                                }
                            }
                        }.trim()
                    }.lparams(dip(150), wrapContent) {
                        endToEnd = R.id.screen_inner_module_widget_01_id_07
                        topToBottom = R.id.screen_inner_module_widget_01_id_07
                    }
                }.lparams(matchConstraint, wrapContent) {
                    startToStart = PARENT_ID
                    endToEnd = PARENT_ID
                    topToBottom = R.id.screen_inner_module_widget_01_id_04
                    bottomToBottom = PARENT_ID
                    topMargin = dip(16)
                    bottomMargin = dip(20)
                }


//                verticalLayout {
//                    id = R.id.screen_inner_module_widget_01_id_05
//                    isVisible = elapsedTime != 0L
//
//                    textView {
//                        id = R.id.screen_inner_module_widget_01_id_06
//                        backgroundResource = R.drawable.ic_graph_num_bg
//                        typeface = Font.latoRegular
//                        textSize = 12f
//                        textColorResource = R.color.x_ffffff
//                        letterSpacing = -0.01f
//                        lines = 1
//                        includeFontPadding = false
//                        gravity = Gravity.CENTER
//                        setLineSpacing(5.8f,0f)
//                        setPadding(0, 0, 0, dip(4.5f))
//                    }.lparams(wrapContent, wrapContent)
//
//                    seekBar {
//                        id = R.id.screen_inner_module_widget_01_id_07
//                        max = 100 + BALLOON_CORRECTION_VALUE
//                        progress = 0
//                        thumb = context.getDrawable(R.drawable.shape_time_bar_thumb)
//                        thumbOffset = dip(7.1f)
//                        progressDrawable = context.getDrawable(R.drawable.layer_list_time_bar)
//                        setPadding(dip(7.1f), 0, dip(7.1f), 0)
//                        onTouch(returnValue = true) { _, _ -> }
//                        onSeekBarChangeListener {
//                            onProgressChanged { seekBar, _, _ ->
//                                seekBar?.let { _seekBar ->
//                                    updatePercentagePosition(_seekBar, result)
//                                }
//                            }
//                        }
//                    }.lparams(dip(281f + 14.2f), wrapContent) {
//                        topMargin = dip(1.0f)
//                        gravity = Gravity.CENTER_HORIZONTAL
//                    }
//
//                    linearLayout {
//                        id = R.id.screen_inner_module_widget_01_id_08
//                        gravity = Gravity.CENTER_VERTICAL
//
//                        nonSpacingTextView(
//                            Spanner().apply {
//                                context.getString(
//                                    R.string.deep_screen_030,
//                                    elapsedTime.millisToFormatString("HH").toInt(),
//                                    elapsedTime.millisToFormatString("mm").toInt(),
//                                    elapsedTime.millisToFormatString("ss").toInt()
//                                ).split('|').let { strings ->
//                                    if (strings.size > 1) {
//                                        val sp = sp(12f)
//                                        val font_1 = Font.CustomTypefaceSpan(Font.mediumByLanguage())
//                                        val font_2 = Font.CustomTypefaceSpan(Font.latoRegular)
//                                        val color_1 = ResourcesCompat.getColor(resources, R.color.x_afafaf, null)
//                                        val color_2= ResourcesCompat.getColor(resources, R.color.x_767676, null)
//                                        append(strings[0], Spans.sizeSP(sp), Spans.custom(font_1), Spans.foreground(color_1))
//                                        append(strings[1], Spans.sizeSP(sp), Spans.custom(font_2), Spans.foreground(color_2))
//                                    }
//                                }
//                            }.trim()
//                        ) {
//                            id = R.id.screen_inner_module_widget_01_id_10
//                            textSize = 12f
//                            lines = 1
//                            includeFontPadding = false
//                            gravity = Gravity.END
//                        }.lparams(wrapContent, wrapContent) {
//                            gravity = Gravity.END
//                           // marginEnd = dip(18.5f)
//                        }
//                    }.lparams(wrapContent, wrapContent) {
//                        gravity = Gravity.CENTER_HORIZONTAL
//                    }
//                }.lparams(matchConstraint, wrapContent) {
//                    startToStart = PARENT_ID
//                    endToEnd = PARENT_ID
//                    topToBottom = R.id.screen_inner_module_widget_01_id_04
//                    bottomToBottom = PARENT_ID
//                    topMargin = dip(10)
//                    bottomMargin = dip(20)
//                }
            }.lparams(matchParent, matchParent)
        }.applyRecursively { v ->
            if (v.id == R.id.screen_inner_module_widget_01_id_07) {
                (v as? SeekBar)?.let { seekBar ->
                    doAsync {
                        calcPercentage().let { p ->
                            seekBar.progress = if (p > 0) p else BALLOON_CORRECTION_VALUE
                        }
                    }
                }
            }
        }
    }

    private fun calcPercentage(): Int {
        return when (elapsedTime != 0L) {
            true -> (focusTime.toDouble() / elapsedTime.toDouble() * 100.0).toInt()
            false -> 0
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updatePercentagePosition(
        seekBar: SeekBar,
        result: Healing
    ) {
        seekBar.thumb.bounds.let { r ->
            (seekBar.parent as View).find<TextView>(R.id.screen_inner_module_widget_01_id_06).let { balloon ->
                ((balloon.width - r.width()) / 2.0f).let { xOffset ->
                    balloon.x = seekBar.x + r.left - xOffset
                }
                balloon.text = "${calcPercentage()}%"
            }
        }
    }

    private fun calcDeepTrainingTimes(result: String): Long {
        return result.let { r ->
            doubleArrayOf().fromJson(r)
        }.let { array ->
            var deepCount = 0
            for (item in array) {
                checkDeepDuring(item)
                    .let { able ->
                        if (able) {
                            deepCount++
                            Timber.i("--> deep : $deepCount")
                        }
                    }
            }
            Timber.d("calcDeepTrainingTimes : $deepCount")
            deepCount.times(2).secondsToMillis()
        }
    }

    companion object {
        const val BALLOON_CORRECTION_VALUE: Int = 2 // Balloon 위치 보정 수치
    }
}