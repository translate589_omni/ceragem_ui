package omnifit.brain.ceragemui.component

import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import omnifit.brain.ceragemui.*
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint


class HeadsetUsageViewComponent<in T : UIFrame> : AnkoComponent<T> {
    lateinit var purchaseButton: Button

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            backgroundColorResource = R.color.x_ffffff
            lparams(matchParent, matchParent)
            // 타이틀
            nonSpacingTextView(R.string.headset_usage_screen_010) {
                id = R.id.screen_widget_id_01
                typeface = Font.mediumByLanguage()
                textSize = 16.0f
                textColorResource = R.color.x_232323
                letterSpacing = -0.04f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(46.0f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 닫기
            imageButton(R.drawable.selector_close_dark) {
                id = R.id.screen_widget_id_02
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(10.0f))
                onDebounceClick {
                    owner.closeQuick()
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                marginEnd = dip(6.7f)
                bottomToBottom = R.id.screen_widget_id_01
                verticalBias = 0.5f
            }

            scrollView {
                id = R.id.screen_widget_id_03

                constraintLayout {
                    id = R.id.screen_widget_id_04
                    setPadding(0,dip(48),0,dip(70))

                    imageView {
                        id = R.id.screen_widget_id_05
                        GlideApp.with(context)
                            .load(R.drawable.img_list_guide)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .dontTransform()
                            .into(this)
                    }.lparams(matchConstraint, wrapContent) {
                        startToStart = PARENT_ID
                        topToTop = PARENT_ID
                        endToEnd = PARENT_ID
                    }

                    purchaseButton = button(R.string.headset_usage_screen_020) {
                        id = R.id.screen_widget_id_06
                        backgroundResource = R.drawable.selector_r24_button_purchase_bg
                        textSize = 16.5f
                        letterSpacing = -0.02f
                        setLineSpacing(2f,0f)
                        typeface = Font.boldByLanguage()
                        elevation = 5.0f
                        isInvisible = true
                        onDebounceClick { moveToExternalLink(context.getString(R.string.purchase_link_url)) }
                    }.lparams(0,dip(55)) {
                        startToStart = PARENT_ID
                        topToBottom = R.id.screen_widget_id_05
                        endToEnd = PARENT_ID
                        topMargin = dip(55)
                        marginStart = dip(18.5f)
                        marginEnd = dip(18.5f)
                    }
                }.lparams(matchParent, wrapContent)

                setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                    val itemView = getChildAt(childCount -1)
                    val diff = itemView.bottom - (height + scrollY)

                    if (diff == 0) { // 스크롤 bottom
                        purchaseButton.isInvisible = false
                        smoothScrollTo(0,itemView.bottom + purchaseButton.bottom)
                    }
                }
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_02
                topMargin = dip(16.0f)
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }
        }
    }
}