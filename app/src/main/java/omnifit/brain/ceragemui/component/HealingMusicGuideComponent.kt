package omnifit.brain.ceragemui.component

import android.graphics.Color
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import omnifit.brain.ceragemui.Font
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.UIFrame
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch

class HealingMusicGuideComponent<T : UIFrame> : AnkoComponent<T> {

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            backgroundColor = Color.WHITE
            onTouch(returnValue = true) { _, _ ->
            }

            // 타이틀
            nonSpacingTextView(R.string.headset_guide_screen_010) {
                id = R.id.screen_widget_id_01
                typeface = Font.mediumByLanguage()
                textSize = 16.0f
                textColorResource = R.color.x_232323
                letterSpacing = -0.04f
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(46.0f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }


            imageView(R.drawable.img_list_guide) {
                id = R.id.screen_widget_id_02
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                topToTop = R.id.screen_widget_id_01
            }

            // 닫기
            imageButton(R.drawable.selector_close_dark) {
                id = R.id.screen_widget_id_03
                onDebounceClick {
                    owner.closeQuick()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                bottomToBottom = R.id.screen_widget_id_01
                marginStart = dip(18.5f)
            }
        }
    }
}