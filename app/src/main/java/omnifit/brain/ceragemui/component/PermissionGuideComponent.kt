package omnifit.brain.ceragemui.component

import android.graphics.Color
import android.view.Gravity
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.Font
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

class PermissionGuideComponent<in T : MaterialDialog> : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            backgroundColor = Color.WHITE

            imageView(R.drawable.ic_service_logo_03) {
                id = R.id.screen_inner_widget_01_id_01
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(39.8f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            nonSpacingTextView(R.string.permission_dialog_010) {
                id = R.id.screen_inner_widget_01_id_02
                typeface = Font.regularByLanguage()
                textSize = 14.0f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                //topToBottom = R.id.screen_inner_widget_01_id_01
                topToTop = PARENT_ID
                topMargin = dip(14.5f + 39.8f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            view {
                id = R.id.screen_inner_widget_01_id_03
                backgroundColorResource = R.color.x_ededed
            }.lparams(dip(266.8f), dip(0.8f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_inner_widget_01_id_02
                topMargin = dip(22.5f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 전화 ------------------------------------------------------------
            imageView(R.drawable.ic_permission_read_phone_state) {
                id = R.id.screen_inner_widget_01_id_04
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_03
                topToBottom = R.id.screen_inner_widget_01_id_03
                topMargin = dip(24.5f)
            }

            nonSpacingTextView(R.string.permission_dialog_020) {
                id = R.id.screen_inner_widget_01_id_05
                typeface = Font.boldByLanguage()
                textSize = 15.0f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_04
                marginStart = dip(13.5f)
                topToTop = R.id.screen_inner_widget_01_id_04
            }

            nonSpacingTextView(R.string.permission_dialog_080) {
                id = R.id.screen_inner_widget_01_id_06
                typeface = Font.regularByLanguage()
                textSize = 12.5f
                textColorResource = R.color.x_32a0f6
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_05
                marginStart = dip(3.0f)
                bottomToBottom = R.id.screen_inner_widget_01_id_05
            }

            nonSpacingTextView(R.string.permission_dialog_030) {
                id = R.id.screen_inner_widget_01_id_07
                typeface = Font.regularByLanguage()
                textSize = 11.5f
                textColorResource = R.color.x_c0c0c0
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_05
                bottomToBottom = R.id.screen_inner_widget_01_id_04
            }
            // -----------------------------------------------------------------

            // 미디어 파일 ------------------------------------------------------
            imageView(R.drawable.ic_permission_read_write_external_storage) {
                id = R.id.screen_inner_widget_01_id_08
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_04
                topToBottom = R.id.screen_inner_widget_01_id_04
                topMargin = dip(17.8f)
            }

            nonSpacingTextView(R.string.permission_dialog_040) {
                id = R.id.screen_inner_widget_01_id_09
                typeface = Font.boldByLanguage()
                textSize = 15.0f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_08
                marginStart = dip(13.5f)
                topToTop = R.id.screen_inner_widget_01_id_08
            }

            nonSpacingTextView(R.string.permission_dialog_080) {
                id = R.id.screen_inner_widget_01_id_10
                typeface = Font.regularByLanguage()
                textSize = 12.5f
                textColorResource = R.color.x_32a0f6
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_09
                marginStart = dip(3.0f)
                bottomToBottom = R.id.screen_inner_widget_01_id_09
            }

            nonSpacingTextView(R.string.permission_dialog_050) {
                id = R.id.screen_inner_widget_01_id_11
                typeface = Font.regularByLanguage()
                textSize = 11.5f
                textColorResource = R.color.x_c0c0c0
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_09
                bottomToBottom = R.id.screen_inner_widget_01_id_08
            }
            // -----------------------------------------------------------------

            // 위치 ------------------------------------------------------------
            imageView(R.drawable.ic_permission_access_coarse_location) {
                id = R.id.screen_inner_widget_01_id_12
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_08
                topToBottom = R.id.screen_inner_widget_01_id_08
                topMargin = dip(17.8f)
            }

            nonSpacingTextView(R.string.permission_dialog_060) {
                id = R.id.screen_inner_widget_01_id_13
                typeface = Font.boldByLanguage()
                textSize = 15.0f
                textColorResource = R.color.x_1c1c1c
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_12
                marginStart = dip(13.5f)
                topToTop = R.id.screen_inner_widget_01_id_12
            }

            nonSpacingTextView(R.string.permission_dialog_080) {
                id = R.id.screen_inner_widget_01_id_14
                typeface = Font.regularByLanguage()
                textSize = 12.5f
                textColorResource = R.color.x_32a0f6
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToEnd = R.id.screen_inner_widget_01_id_13
                marginStart = dip(3.0f)
                bottomToBottom = R.id.screen_inner_widget_01_id_13
            }

            nonSpacingTextView(R.string.permission_dialog_070) {
                id = R.id.screen_inner_widget_01_id_15
                typeface = Font.regularByLanguage()
                textSize = 11.5f
                textColorResource = R.color.x_c0c0c0
                letterSpacing = -0.03f
                includeFontPadding = false
                lines = 1
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_01_id_13
                bottomToBottom = R.id.screen_inner_widget_01_id_12
            }
            // -----------------------------------------------------------------


            button(R.string.common_010) {
                id = R.id.screen_inner_widget_01_id_16
                backgroundResource = R.drawable.selector_button_bg
                textSize = 14.5f
                letterSpacing = -0.03f
                typeface = Font.boldByLanguage()
                setTextColor(
                    AppCompatResources.getColorStateList(
                        context,
                        R.color.selector_button_text
                    )
                )
                onDebounceClick {
                    owner.dismiss()
                }
            }.lparams(matchConstraint, dip(51.5f)) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_inner_widget_01_id_12
                topMargin = dip(42.0f)
                endToEnd = PARENT_ID
            }
        }
    }
}