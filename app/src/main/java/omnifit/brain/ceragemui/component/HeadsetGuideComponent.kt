package omnifit.brain.ceragemui.component

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import omnifit.brain.ceragemui.Font
import omnifit.brain.ceragemui.GlideApp
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.UIFrame
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch

class HeadsetGuideComponent<T : UIFrame> : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            backgroundColor = Color.WHITE
            onTouch(returnValue = true) { _, _ ->
            }

            imageView {
                GlideApp.with(context)
                    .load(R.drawable.img_guide_04)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .fitCenter()
                    .into(this)
            }.lparams(0, 0) {
                topToTop = PARENT_ID
                bottomToBottom = PARENT_ID
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
            }

            // 타이틀
            nonSpacingTextView(R.string.headset_guide_screen_010) {
                id = R.id.screen_widget_id_01
                typeface = Font.mediumByLanguage()
                textSize = 16.0f
                textColorResource = R.color.x_232323
                letterSpacing = -0.03f
                lines = 1
                setLineSpacing(21.5f,0f)
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(46.0f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 닫기
            imageButton(R.drawable.selector_close_dark) {
                onDebounceClick {
                    owner.closeQuick()
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                marginEnd = dip(17.3f)
                bottomToBottom = R.id.screen_widget_id_01
            }
        }
    }
}