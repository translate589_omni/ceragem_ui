package omnifit.brain.ceragemui.component

import android.view.Gravity
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_SPREAD
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import com.afollestad.materialdialogs.MaterialDialog
import omnifit.brain.ceragemui.Font
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

class BasicPopComponent<in T : MaterialDialog> constructor(
    private val title: String,
    private val message: String,
    private val buttonActionListener: ButtonActionListener,
    private val isUseNegative: Boolean = true
) : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.dialog_widget_01_id_root_container
            backgroundResource = R.drawable.shape_pop_bg

            // 타이틀
            nonSpacingTextView {
                id = R.id.dialog_widget_01_id_01
                typeface = Font.mediumByLanguage()
                textSize = 25.5f
                textColorResource = R.color.x_338fe6
                letterSpacing = -0.08f
                lines = 1
                includeFontPadding = false
                text = title
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(36.8f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 메세지
            nonSpacingTextView {
                id = R.id.dialog_widget_01_id_02
                typeface = Font.regularByLanguage()
                textSize = 13.0f
                textColorResource = R.color.x_6f6f6f
                letterSpacing = -0.05f
                includeFontPadding = false
                gravity = Gravity.CENTER
                text = message
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.dialog_widget_01_id_01
                topMargin = dip(32.0f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 취소
            button(R.string.common_020) {
                id = R.id.dialog_widget_01_id_03
                backgroundResource = R.drawable.selector_pop_negative_button_bg
                typeface = Font.mediumByLanguage()
                textSize = 18.0f
                textColorResource = R.color.x_646466
                letterSpacing = -0.05f
                lines = 1
                includeFontPadding = false
                gravity = Gravity.CENTER
                onDebounceClick {
                    buttonActionListener.onNegative()
                }
            }.lparams(matchConstraint, dip(49.2f)) {
                startToStart = PARENT_ID
                marginStart = dip(17.5f)
                topToBottom = R.id.dialog_widget_01_id_02
                topMargin = dip(41.2f)
                endToStart = R.id.dialog_widget_01_id_04
                bottomToBottom = PARENT_ID
                bottomMargin = dip(25.0f)
                horizontalChainStyle = CHAIN_SPREAD
                horizontalBias = 0.5f
                horizontalWeight = if (isUseNegative) 1.0f else 0.0f
            }

            // 확인
            button(R.string.common_010) {
                id = R.id.dialog_widget_01_id_04
                backgroundResource = R.drawable.selector_pop_positive_button_bg
                typeface = Font.mediumByLanguage()
                textSize = 18.0f
                textColorResource = R.color.x_ffffff
                letterSpacing = -0.05f
                lines = 1
                includeFontPadding = false
                gravity = Gravity.CENTER
                onDebounceClick {
                    buttonActionListener.onPositive()
                }
            }.lparams(matchConstraint, dip(49.2f)) {
                startToEnd = R.id.dialog_widget_01_id_03
                if (isUseNegative) {
                    marginStart = dip(11.2f)
                }
                topToTop = R.id.dialog_widget_01_id_03
                endToEnd = PARENT_ID
                marginEnd = dip(17.5f)
                bottomToBottom = R.id.dialog_widget_01_id_03
                verticalBias = 0.5f
                horizontalWeight = 1.0f
            }
        }
    }

    interface ButtonActionListener {
        fun onPositive()
        fun onNegative()
    }
}