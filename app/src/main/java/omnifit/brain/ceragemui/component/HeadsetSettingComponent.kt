package omnifit.brain.ceragemui.component

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.provider.Settings
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.CHAIN_PACKED
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.afollestad.materialdialogs.MaterialDialog
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple
import com.rodolfonavalon.shaperipplelibrary.model.Circle
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import omnifit.brain.ceragemui.*
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.view.onDebounceClick
import omnifit.brain.ceragemui.view.RotateLoading
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.rotateLoading
import omnifit.brain.ceragemui.view.shapeRipple
import omnifit.sdk.bluetooth.*
import omnifit.sdk.omnifitbrain.FindState
import omnifit.sdk.omnifitbrain.HeadsetProperties
import omnifit.sdk.omnifitbrain.LinkState
import omnifit.sdk.omnifitbrain.OmnifitBrain
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onTouch
import timber.log.Timber

class HeadsetSettingComponent<in T : UIFrame> : AnkoComponent<T>, DrawerLayout.DrawerListener {
    private var purchaseBanner: CardView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.screen_inner_widget_05_id_root_container
            backgroundColorResource = R.color.x_ffffff
            layoutTransition = LayoutTransition()
            onTouch(returnValue = true) { _, _ ->
            }

            // 타이틀
            nonSpacingTextView(R.string.headset_setting_010) {
                id = R.id.screen_inner_widget_05_id_01
                typeface = Font.mediumByLanguage()
                textSize = 16.0f
                textColorResource = R.color.x_232323
                letterSpacing = -0.03f
                lines = 1
                setLineSpacing(21.5f,0f)
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(46.5f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // 닫기
            imageButton(R.drawable.selector_close_dark) {
                id = R.id.screen_inner_widget_05_id_02
                scaleType = ImageView.ScaleType.FIT_CENTER
                isVisible = false
                setPadding(dip(10.0f))
                bindLinkStateVisibleChangeObserver(owner)
                onDebounceClick {
                    owner.closeHeadsetSetting()
                }
            }.lparams(dip(38), dip(38)) {
                topToTop = R.id.screen_inner_widget_05_id_01
                endToEnd = PARENT_ID
                marginEnd = dip(7.3f)
                bottomToBottom = R.id.screen_inner_widget_05_id_01
            }

            // 진행 메세지
            textSwitcher {
                id = R.id.screen_inner_widget_05_id_03
                setFactory {
                    TextView(context).apply {
                        typeface = Font.regularByLanguage()
                        textSize = 14.0f
                        textColorResource = R.color.x_1c1c1c
                        letterSpacing = -0.03f
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
//                setCurrentText(context.getString(R.string.headset_setting_020))
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                topMargin = dip(125.5f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            // Finding
            shapeRipple {
                id = R.id.screen_inner_widget_05_id_04
                rippleShape = Circle()
                rippleColor = ContextCompat.getColor(context, R.color.x_1b7ae2)
                rippleCount = 2
                rippleDuration = 1200
                rippleStrokeWidth = dip(3.5f)
                isEnableStrokeStyle = true
//                isEnableSingleRipple = true
                isEnableColorTransition = true
                isVisible = false
            }.lparams(owner.width, owner.width) {
                startToStart = R.id.screen_inner_widget_05_id_05
                topToTop = R.id.screen_inner_widget_05_id_05
                endToEnd = R.id.screen_inner_widget_05_id_05
                bottomToBottom = R.id.screen_inner_widget_05_id_06
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // Linking
            rotateLoading {
                id = R.id.screen_inner_widget_05_id_09
                setStrokeWidth(dip(3.5f))
                setLoadingColor(ContextCompat.getColor(context, R.color.x_32a0f6))
            }.lparams(dip(280.0f), dip(280.0f)) {
                startToStart = R.id.screen_inner_widget_05_id_05
                topToTop = R.id.screen_inner_widget_05_id_05
                endToEnd = R.id.screen_inner_widget_05_id_05
                bottomToBottom = R.id.screen_inner_widget_05_id_06
                horizontalBias = 0.5f
                verticalBias = 0.5f
            }

            // 헤드셋 연결 상태
            imageSwitcher {
                id = R.id.screen_inner_widget_05_id_05
                setFactory {
                    ImageView(context).apply {
                        imageResource = connectionStateDrawables[LinkDrawableState.UNREGISTERED.ordinal]
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToTop = R.id.screen_inner_widget_05_id_06
                verticalChainStyle = CHAIN_PACKED
                horizontalBias = 0.5f
                verticalBias = 0.45f
            }

            // 헤드셋 장치명
            textSwitcher {
                id = R.id.screen_inner_widget_05_id_06
                setFactory {
                    TextView(context).apply {
                        typeface = Font.latoRegular
                        textSize = 13.6f
                        setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_headset_name_text))
                        letterSpacing = -0.01f
                        lines = 1
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                        isEnabled = false
                    }
                }
                inAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
                outAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
                setCurrentText(context.getString(R.string.headset_setting_060))
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_05_id_05
                topToBottom = R.id.screen_inner_widget_05_id_05
                topMargin = dip(13.2f)
                endToEnd = R.id.screen_inner_widget_05_id_05
                bottomToBottom = PARENT_ID
                bottomMargin = dip(15.0f)
                horizontalBias = 0.5f
            }

            // 헤드셋 연결하기
            button(R.string.headset_setting_980) {
                id = R.id.screen_inner_widget_05_id_07
                backgroundResource = R.drawable.selector_headset_connect_button_bg
                typeface = Font.regularByLanguage()
                textSize = 14.5f
                setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_headset_connect_button_text))
                letterSpacing = -0.05f
                lines = 1
                includeFontPadding = false
                gravity = Gravity.CENTER
                isVisible = false
                onDebounceClick {
                    if (BluetoothAdapter.getDefaultAdapter().isEnabled) link()
                    else requestBluetoothAdapterEnable(context)
                }
            }.lparams(dip(93.5f), dip(26.5f)) {
                startToStart = R.id.screen_inner_widget_05_id_06
                topToBottom = R.id.screen_inner_widget_05_id_06
                topMargin = dip(10.0f)
                endToEnd = R.id.screen_inner_widget_05_id_06
                horizontalBias = 0.5f
            }

            purchaseBanner = cardView {
                radius = dip(12.5f).toFloat()
                cardElevation = dip(2.5f).toFloat()
                setCardBackgroundColor(Color.WHITE)

                imageButton(R.drawable.selector_purchase_link_banner) {
                    id = R.id.screen_inner_widget_05_id_10
                    backgroundResource = R.drawable.link_banner_l
                    onDebounceClick {
                        moveToExternalLink(context.getString(R.string.purchase_link_url))
                    }
                }.lparams(wrapContent, wrapContent)
                isVisible = HeadsetProperties.get(context).isEmpty()
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_05_id_05
                endToEnd = R.id.screen_inner_widget_05_id_05
                bottomToTop = R.id.screen_inner_widget_05_id_08
                bottomMargin = dip(15.0f)
            }

            // 배터리 잔량 S
            nonSpacingTextView(R.string.headset_setting_070) {
                id = R.id.screen_inner_widget_05_id_11
                typeface = Font.mediumByLanguage()
                textSize = 11.0f
                textColorResource = R.color.x_767676
                letterSpacing = -0.05f
                lines = 1
                setLineSpacing(7.5f,0f)
                includeFontPadding = false
                bindLinkStateVisibleChangeObserver(owner)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_inner_widget_05_id_06
                topMargin = dip(20f)
                endToEnd = PARENT_ID
                horizontalBias = 0.5f
            }

            horizontalProgressBar {
                id = R.id.screen_inner_widget_05_id_12
                progressDrawable = ContextCompat.getDrawable(context, R.drawable.layer_list_battery)
                max = 100
                bindBatteryLevelChangeObserver(owner)
                bindLinkStateVisibleChangeObserver(owner)
            }.lparams(dip(51.8f), dip(25.3f)) {
                topMargin = dip(8.8f)
                startToStart = R.id.screen_inner_widget_05_id_11
                endToStart = R.id.screen_inner_widget_05_id_13
                topToBottom = R.id.screen_inner_widget_05_id_11
            }

            nonSpacingTextView {
                id = R.id.screen_inner_widget_05_id_13
                typeface = Font.latoBold
                textSize = 24f
                textColorResource = R.color.x_000000
                lines = 1
                includeFontPadding = false
                bindBatteryLevelChangeObserver(owner)
                bindLinkStateVisibleChangeObserver(owner)
                setLineSpacing(-8.6f,0f)
            }.lparams(wrapContent, wrapContent) {
                marginStart = dip(10)
                startToEnd = R.id.screen_inner_widget_05_id_12
                topToTop = R.id.screen_inner_widget_05_id_12
                endToEnd = R.id.screen_inner_widget_05_id_11
                bottomToBottom = R.id.screen_inner_widget_05_id_12
            }
            // 배터리 잔량 E

            // 변경하기
            button(R.string.headset_setting_990) {
                id = R.id.screen_inner_widget_05_id_08
                backgroundResource = R.drawable.selector_r24_button_bg
                textSize = 15.5f
                letterSpacing = -0.03f
                typeface = Font.regularByLanguage()
                elevation = 5.0f
                setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_r24_button_text))
                onDebounceClick {
                    if (!OmnifitBrain.isLinking()) {
                        find()
                    }
                }
            }.lparams(dip(323.0f), dip(46.0f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(19.5f)
                horizontalBias = 0.5f
            }
        }
    }.apply {
        handlingHeadsetProperties = HeadsetProperties.get(context)
        if (linkStateChangeObservableFinalizer == null) {
            observeLinkStateChange()
        }
    }

    override fun onDrawerStateChanged(newState: Int) {}

    override fun onDrawerSlide(nv: View, slideOffset: Float) {}

    @SuppressLint("CheckResult")
    override fun onDrawerOpened(nv: View) {
        if (nv.id == R.id.frame_widget_id_03) {
            HeadsetProperties.get(nv.context).let { prop ->
                handlingHeadsetProperties = prop
                if (BluetoothAdapter.getDefaultAdapter().isEnabled && prop.isEmpty()) {
                    nv.find()
                }
            }
        }
    }

    override fun onDrawerClosed(nv: View) {
        if (OmnifitBrain.isFinding()) {
            OmnifitBrain.stopFind()
        }
        with(HeadsetProperties.get(nv.context)) {
            //UNREGISTER
            if (isEmpty()) {
                with(nv) {
                    updateProgressMessage()
                    updateLinkStateDrawable()
                    updateDeviceName()
                    updateDeviceNameFocusable(false)
                    updateConnectButtonVisible(false)
                    updateCircularRippleVisible(false)
                    updateRotateLoadingVisible(false)
                }
            }
            else {
                handlingHeadsetProperties = this
                nv.updateUIByLinkState(OmnifitBrain.linkState())
            }
        }
        linkStateChangeObservableFinalizer?.dispose()
        linkStateChangeObservableFinalizer = null
    }

    private fun requestBluetoothAdapterEnable(context: Context) {
        context.startActivity(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
    }

    private val connectionStateDrawables: Array<Int> by lazy {
        arrayOf(
            R.drawable.img_headset_all_basic,
            R.drawable.img_headset_earphone_all_off,
            R.drawable.img_headset_earphone_all_off,
            R.drawable.img_headset_earphone_all_off,
            R.drawable.img_headset_all_on
        )
    }

    private var handlingHeadsetProperties: HeadsetProperties = HeadsetProperties()
    private var linkStateChangeObservableFinalizer: Disposable? = null

    @Suppress("CheckResult")
    private fun View.observeLinkStateChange() {
        linkStateChangeObservableFinalizer = OmnifitBrain.observeLinkStateChange()
            .subscribeOn(Schedulers.io())
            .bindUntilEvent(context as LifecycleOwner, Lifecycle.Event.ON_DESTROY)
            .doFinally {
                linkStateChangeObservableFinalizer?.dispose()
                linkStateChangeObservableFinalizer = null
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { state ->
                    Timber.e("--> [헤드셋 설정] 링크 : (${state.hashCode()})$state")
                    if (state is LinkState.Error) {
                        when (state.throwable) {
                            is BluetoothA2dpNotFoundException,
                            is BluetoothA2dpConnectionFailureException -> {
                                (context as? UIFrame)?.alert(context, R.string.headset_setting_080)
                            }
                        }
                    }
                    updateUIByLinkState(state)
                },
                { e ->
                    updateCircularRippleVisible(false)
                    updateRotateLoadingVisible(false)
                    Timber.e(e)
                }
            )
    }

    @Suppress("CheckResult")
    private fun View.find() {
        OmnifitBrain.find()
            .subscribeOn(Schedulers.io())
            .bindUntilEvent(context as LifecycleOwner, Lifecycle.Event.ON_DESTROY)
            .doOnSubscribe { OmnifitBrain.stopFind() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { state ->
                    when (state) {
                        is FindState.Start  -> {
                            updateProgressMessage(R.string.headset_setting_030)
                            updateLinkStateDrawable(LinkDrawableState.UNREGISTERED.ordinal)
                            updateDeviceName()
                            updateDeviceNameFocusable(false)
                            updateConnectButtonVisible(false)
                            updateCircularRippleVisible(true)
                        }
                        is FindState.Found  -> {
                            handlingHeadsetProperties = state.result
                            updateProgressMessage()
                            updateLinkStateDrawable(LinkDrawableState.UNLINKED.ordinal)
                            updateDeviceName(handlingHeadsetProperties)
                            updateDeviceNameFocusable(false)
                            updateConnectButtonFocusable(true)
                            updateConnectButtonVisible(true)
                            updateCircularRippleVisible(false)
                        }
                        is FindState.Finish -> {
                            if (context.isHeadsetSettingOpened()) {
                                updateProgressMessage(R.string.headset_setting_040)
                                updateDeviceName()
                                updateConnectButtonVisible(false)
                                updateCircularRippleVisible(false)
                            }
                        }
                    }
                },
                { e ->
                    Timber.e(e)
                    // TODO : 블루투스 예외 UI 처리부
                    when (e) {
                        is BluetoothNotAvailableException        -> {}
                        is LocationPermissionNotGrantedException -> {}
                        is BluetoothNotEnabledException          -> {
                            requestBluetoothAdapterEnable(context)
                        }
                        is LocationServicesNotEnabledException   -> {
                            MaterialDialog(context)
                                .show {
                                    hideNavigation()
                                    icon(R.mipmap.ic_launcher)
                                    message(R.string.notifications_050)
                                    positiveButton(R.string.dialog_010) {
                                        context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                                        dismiss()
                                    }
                                    negativeButton(R.string.dialog_020)
                                    cancelable(false)
                                    cancelOnTouchOutside(false)
                                }
                        }
                        else                                     -> {}
                    }
                }
            )
    }

    private fun View.link() {
        handlingHeadsetProperties.run {
            if (!isEmpty()) {
                updateProgressMessage()
                updateCircularRippleVisible(false)
                updateRotateLoadingVisible(true)
                updateConnectButtonFocusable(false)
                OmnifitBrain.link(context, this)
            }
        }
    }

    private fun Context.isHeadsetSettingOpened(): Boolean {
        return (this as? UIFrame)?.isQuickOpened() ?: false
    }

    private fun View.updateUIByLinkState(state: LinkState) {
        when (state) {
            is LinkState.Unlinked        -> {
                if (!handlingHeadsetProperties.isEmpty()) {
                    updateProgressMessage(if (OmnifitBrain.isLinking()) "" else context.getString(R.string.headset_setting_020))
                    updateLinkStateDrawable(LinkDrawableState.UNLINKED.ordinal)
                    updateDeviceName(handlingHeadsetProperties)
                    updateDeviceNameFocusable(false)
                    updateConnectButtonFocusable(true)
                    updateConnectButtonVisible(true)
                    updateCircularRippleVisible(false)
                    updateRotateLoadingVisible(OmnifitBrain.isLinking())
                }
            }
            is LinkState.EegSensorLinked -> {
                updateProgressMessage(if (OmnifitBrain.isLinking()) "" else context.getString(R.string.headset_setting_020))
                updateLinkStateDrawable(LinkDrawableState.EARPHONE_UNLINKED.ordinal)
                updateDeviceName(handlingHeadsetProperties)
                updateDeviceNameFocusable(false)
                updateConnectButtonFocusable(!OmnifitBrain.isLinking())
                updateConnectButtonVisible(true)
            }
            is LinkState.EarphoneLinked  -> {
                updateProgressMessage(if (OmnifitBrain.isLinking()) "" else context.getString(R.string.headset_setting_020))
                updateLinkStateDrawable(LinkDrawableState.EEG_SENSOR_UNLINKED.ordinal)
                updateDeviceName(handlingHeadsetProperties)
                updateDeviceNameFocusable(false)
                updateConnectButtonFocusable(!OmnifitBrain.isLinking())
                updateConnectButtonVisible(true)
            }
            is LinkState.Linked          -> {
                updateProgressMessage()
                updateLinkStateDrawable(LinkDrawableState.LINKED.ordinal)
                updateDeviceName(handlingHeadsetProperties)
                updateDeviceNameFocusable(true)
                updateConnectButtonVisible(false)
                updateCircularRippleVisible(false)
                updateRotateLoadingVisible(false)
            }
            is LinkState.Error           -> {
                updateConnectButtonFocusable(true)
                updateConnectButtonVisible(true)
                updateCircularRippleVisible(false)
                updateRotateLoadingVisible(false)
            }
        }
    }

    private fun View.updateProgressMessage(@StringRes msg: Int) {
        updateProgressMessage(context.getString(msg))
    }

    private fun View.updateProgressMessage(msg: String = "") {
        parent?.run {
            (this as? View)?.run {
                find<TextSwitcher>(R.id.screen_inner_widget_05_id_03).setText(msg)
            }
        }
    }

    private fun View.updateLinkStateDrawable(level: Int = LinkDrawableState.UNREGISTERED.ordinal) {
        parent?.run {
            (this as? View)?.run {
                find<ImageSwitcher>(R.id.screen_inner_widget_05_id_05).setImageResource(connectionStateDrawables[level])
            }
        }
    }

    private fun View.updateDeviceName(properties: HeadsetProperties? = null) {
        parent?.run {
            (this as? View)?.run {
                find<TextSwitcher>(R.id.screen_inner_widget_05_id_06).setText(properties?.name ?: context.getString(R.string.headset_setting_060))
            }
        }
    }

    private fun View.updateDeviceNameFocusable(enabled: Boolean = false) {
        parent?.run {
            (this as? View)?.run {
                find<TextSwitcher>(R.id.screen_inner_widget_05_id_06).currentView?.isEnabled = enabled
            }
        }
    }

    private fun View.updateCircularRippleVisible(visible: Boolean = false) {
        parent?.run {
            (this as? View)?.run {
                //            find<ProgressBar>(R.id.screen_inner_widget_05_id_04).isVisible = visible
                find<ShapeRipple>(R.id.screen_inner_widget_05_id_04).isVisible = visible
            }
        }
    }

    private fun View.updateRotateLoadingVisible(visible: Boolean = false) {
        parent?.run {
            (this as? View)?.run {
                find<RotateLoading>(R.id.screen_inner_widget_05_id_09).run {
                    if (visible) start()
                    else stop()
                }
            }
        }
    }

    private fun View.updateConnectButtonVisible(visible: Boolean = false) {
        parent?.run {
            (this as? View)?.run {
                find<Button>(R.id.screen_inner_widget_05_id_07).isVisible = visible
            }
        }
    }

    private fun View.updateConnectButtonFocusable(enabled: Boolean = true) {
        parent?.run {
            (this as? View)?.run {
                find<Button>(R.id.screen_inner_widget_05_id_07).isEnabled = enabled
            }
        }
    }

    enum class LinkDrawableState {
        UNREGISTERED,
        UNLINKED,
        EARPHONE_UNLINKED,
        EEG_SENSOR_UNLINKED,
        LINKED
    }
}