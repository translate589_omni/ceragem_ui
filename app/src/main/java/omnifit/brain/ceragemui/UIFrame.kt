package omnifit.brain.ceragemui

//import omnifit.brain.ceragemui.screen.LoginScreen
import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_CLOSE_SYSTEM_DIALOGS
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.collection.SparseArrayCompat
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.navigation.NavigationView
import com.tedpark.tedpermission.rx2.TedRx2Permission
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import es.dmoral.toasty.Toasty
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.ceragemui.UIFrame.OnDrawerStateChangeListener.Companion.STATE_CLOSED
import omnifit.brain.ceragemui.UIFrame.OnDrawerStateChangeListener.Companion.STATE_OPENED
import omnifit.brain.ceragemui.algorithm.ServiceCategory
import omnifit.brain.ceragemui.algorithm.loadIndicatorLabels
import omnifit.brain.ceragemui.component.*
import omnifit.brain.ceragemui.helper.ContentHelper
import omnifit.brain.ceragemui.others.BrainEventListener
import omnifit.brain.ceragemui.screen.MainScreen
import omnifit.sdk.common.BaseObservableProperty
import omnifit.sdk.omnifitbrain.HeadsetProperties
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.support.v4.drawerLayout
import timber.log.Timber

class UIFrame : AppCompatActivity() {
    private var drawer: DrawerLayout? = null
    private var nv: NavigationView? = null
    private var headsetSettingComponent: HeadsetSettingComponent<UIFrame>? = null
    private var onDrawerStateChangeListener: OnDrawerStateChangeListener? = null
    private var onPlaybackStateChangeListener: OnPlaybackStateChangeListener? = null
    private val keyEventReceiver: BroadcastReceiver by lazy {
        object : BroadcastReceiver() {
            val SYSTEM_DIALOG_REASON_KEY = "reason"
            val SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions"
            val SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps"
            val SYSTEM_DIALOG_REASON_HOME_KEY = "homekey"
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    ACTION_CLOSE_SYSTEM_DIALOGS -> {
                        when (intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY)) {
                            SYSTEM_DIALOG_REASON_HOME_KEY -> attachedScreen?.onHomePressed()
                            SYSTEM_DIALOG_REASON_RECENT_APPS -> attachedScreen?.onRecentAppsPressed()
                        }
                    }
                }
            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        requireDensity()
        registerReceiver(keyEventReceiver, IntentFilter(ACTION_CLOSE_SYSTEM_DIALOGS))

        // 앱 실행 시 이미 연결 된 헤드셋이 있었는지 체크
        val hasNoHeadsetProperties = HeadsetProperties.get(applicationContext).isEmpty()

        super.onCreate(savedInstanceState)
        drawer = drawerLayout {
            setScrimColor(Color.TRANSPARENT)
            setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            addDrawerListener(object : DrawerLayout.DrawerListener {
                override fun onDrawerStateChanged(newState: Int) {}
                override fun onDrawerSlide(nv: View, slideOffset: Float) {}
                override fun onDrawerClosed(nv: View) {
                    onDrawerStateChangeListener?.onStateChanged(this@UIFrame, STATE_CLOSED)
                    if ((nv.tag as? Int) == NV_HEADSET_SETTING) {
                        headsetSettingComponent?.onDrawerClosed(nv)

                        HeadsetProperties.get(applicationContext).takeUnless {
                            it.isEmpty() || !hasNoHeadsetProperties
                        }?.run {
                            openHeadsetGuide(applicationContext)
                        }
                    }

                    nv.tag = null
                }

                override fun onDrawerOpened(nv: View) {
                    onDrawerStateChangeListener?.onStateChanged(this@UIFrame, STATE_OPENED)
                    if ((nv.tag as? Int) == NV_HEADSET_SETTING) {
                        headsetSettingComponent?.onDrawerOpened(nv)
                    }
                }
            })

            constraintLayout {
                frameLayout {
                    id = R.id.screen_container
                }.lparams(matchConstraint, matchConstraint) {
                    startToStart = PARENT_ID
                    topToTop = PARENT_ID
                    endToEnd = PARENT_ID
                    bottomToBottom = PARENT_ID
                }
            }

            //헤드셋 설정
            nv = navigationView {
                id = R.id.frame_widget_id_03
            }.lparams(this@UIFrame.width, matchParent) {
                gravity = GravityCompat.END
            }
        }

        loadIndicatorLabels()

        screenTo(MainScreen(),R.anim.slide_hold,R.anim.slide_hold)
    }

    private val attachedScreenCache: SparseArrayCompat<Class<out UIScreen>> = SparseArrayCompat()
    var attachedScreen: UIScreen? = null
    override fun onAttachFragment(fragment: Fragment) {
        attachedScreen?.let { screen ->
            attachedScreenCache.put(attachedScreenCache.size(), screen::class.java)
        }
        (fragment as? UIScreen)?.let { screen ->
            screen::class.java.let { `class` ->
                if (attachedScreenCache.containsValue(`class`)) {
                    attachedScreenCache.indexOfValue(`class`).let { n ->
                        attachedScreenCache.removeAtRange(n, attachedScreenCache.size() - n)
                    }
                }
            }
            attachedScreen = screen
            onDrawerStateChangeListener = screen
            onPlaybackStateChangeListener = screen
        }
    }

    override fun onUserLeaveHint() {
        Timber.e("--> OnUserLeaveHint")
        attachedScreen?.onUserLeaveHint()
    }

    override fun onDestroy() {
        unregisterReceiver(keyEventReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (isQuickOpened()) {
            closeQuick()
            return
        }
        attachedScreen?.let { screen ->
            if (screen.onBackPressed()) return
            if (!screen.isRootScreen && !attachedScreenCache.isEmpty) {
                attachedScreenCache[attachedScreenCache.size() - 1]
                    ?.newInstance()
                    ?.let { backScreen ->
                        screenTo(backScreen)
                    }
                return
            }
        }
        super.onBackPressed()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            keepScreenOn()
            layoutNoLimits()
            hideNavigation()
            attachedScreen?.onWindowFocusChanged(hasFocus)
            updateBindables(applicationContext)
        }
    }

    /**
     * @return px
     */
    val width: Int
        get() = resources.displayMetrics.widthPixels

    /**
     * @return px
     */
    val height: Int
        get() = (resources.displayMetrics.heightPixels + navigationBarHeight)

    /**
     * @return px
     */
    val statusBarHeight: Int
        get() = identifier(ID_NAME_STATUS_BAR_HEIGHT, DEF_TYPE_DIMEN, DEF_PACKAGE_ANDROID)
            .let { id ->
                if (id != 0) dimen(id) else 0
            }

    /**
     * @return px
     */
    val navigationBarHeight: Int
        get() = identifier(ID_NAME_NAVIGATION_BAR_HEIGHT, DEF_TYPE_DIMEN, DEF_PACKAGE_ANDROID)
            .let { id ->
                if (id != 0) dimen(id) else 0
            }

    val dpi: Int
        get() = resources.displayMetrics.densityDpi

    private var temporaryBackground: Drawable? = null

    fun isHeadsetSettingOpened(): Boolean {
        return nv?.let { nv ->
            (nv.tag as? Int) == NV_HEADSET_SETTING
        } ?: false
//        return isDrawerOpened(NV_HEADSET_SETTING)
    }

    fun openHeadsetSetting(context: Context) {
        nv?.let { nv ->
            nv.removeAllViews()
            HeadsetSettingComponent<UIFrame>()
                .also { c ->
                    headsetSettingComponent = c
                }
                .createView(AnkoContext.create(context, this@UIFrame))
                .also { v ->
                    nv.addView(v)
                    nv.tag = NV_HEADSET_SETTING
                }
            openDrawer(NV_HEADSET_SETTING)
        }
    }

    fun closeHeadsetSetting() {
        nv?.let { nv ->
            if ((nv.tag as? Int) == NV_HEADSET_SETTING) {
                closeDrawer(NV_HEADSET_SETTING)
            }
        }
    }

    fun openHeadsetGuide(context: Context) {
        nv?.let { nv ->
            nv.removeAllViews()
            HeadsetGuideComponent<UIFrame>()
                .createView(AnkoContext.create(context, this@UIFrame))
                .also { v ->
                    nv.addView(v)
                    nv.tag = NV_HEADSET_GUIDE
                }
            openDrawer(NV_HEADSET_GUIDE)
        }
    }
//
//    fun openHeadsetUsage(contextContext) {
//        HeadsetUsageViewComponent
//    }

    fun openHealingMusicGuide(context: Context) {
        nv?.let { nv ->
            nv.removeAllViews()
            HeadsetUsageViewComponent<UIFrame>()
                .createView(AnkoContext.create(context, this@UIFrame))
                .also { v ->
                    nv.addView(v)
                    nv.tag = NV_HEALING_MUSIC_GUIDE
                }
            openDrawer(NV_HEALING_MUSIC_GUIDE)
        }
    }

    fun lockDrawer() {
        drawer?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    fun unlockDrawer() {
        drawer?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    fun changeToDarkStatusBar() {
//        window.decorView.systemUiVisibility =
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                DEFAULT_SYSTEM_UI_FLAG or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//            }
//            else DEFAULT_SYSTEM_UI_FLAG
    }

    fun changeToLightStatusBar() {
        window.decorView.systemUiVisibility = DEFAULT_SYSTEM_UI_FLAG
    }

    /**
     * 서비스 카테고리 지정자
     */
    var currentServiceCategory: ServiceCategory
        set(value) {
            (application as CeragemApplication).currentServiceCategory = value
        }
        get() = (application as CeragemApplication).currentServiceCategory

    fun alert(
        context: Context,
        message: String,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = Toasty.custom(
        context,
        message,
        null,
        ContextCompat.getColor(context, colorRes),
        Toast.LENGTH_SHORT,
        false,
        true
    ).apply { setGravity(gravity, 0, 0) }.show()

    // TODO : 임시 토스트
    fun alert(
        context: Context,
        messageRes: Int,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = alert(context, context.getString(messageRes), colorRes, gravity)

    fun isQuickOpened(): Boolean {
        return drawer?.let { q ->
            q.isDrawerOpen(GravityCompat.START).let { on ->
                if (on) on
                else q.isDrawerOpen(GravityCompat.END)
            }
        } ?: false
    }

    fun closeQuick() {
        if (isQuickOpened()) {
            drawer?.closeDrawers()
        }
    }

    private fun keepScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    // Translate589 190903 이 flag 가 추가 되면 adjustPan 이 먹히지 않는다.
    private fun layoutNoLimits() {
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    fun hideNavigation() {
        window.decorView.systemUiVisibility = DEFAULT_SYSTEM_UI_FLAG
    }

    fun hideSoftKeyboard() {
        val view = currentFocus
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun requireDensity() {
        (application as CeragemApplication).desiredDisplayMetrics?.let { metrics ->
            resources.displayMetrics.setTo(metrics)
            Timber.e("--> THIS[$this] FONT SCALE[${resources.configuration.fontScale}]")
        }
    }

    private fun isDrawerOpened(quick: Int): Boolean {
        return drawer?.isDrawerOpen(
            when (quick) {
                NV_MENU -> GravityCompat.START
                else -> GravityCompat.END
            }
        ) ?: false
    }

    private fun openDrawer(quick: Int) {
        drawer?.openDrawer(
            when (quick) {
                NV_MENU -> GravityCompat.START
                else -> GravityCompat.END
            }
        )
    }

    private fun closeDrawer(quick: Int) {
        drawer?.closeDrawer(
            when (quick) {
                NV_MENU -> GravityCompat.START
                else -> GravityCompat.END
            }
        )
    }

    private fun updateBindables(context: Context) {
        // 알림 설정 변화
        //Bindable.notificationEnabled.value = AppConfig.isPersonalizedFeaturedServiceNotificationEnabled(context)
    }

    companion object {
        const val ID_NAME_STATUS_BAR_HEIGHT: String = "status_bar_height"
        const val ID_NAME_NAVIGATION_BAR_HEIGHT: String = "navigation_bar_height"
        const val DEF_TYPE_DIMEN: String = "dimen"
        const val DEF_PACKAGE_ANDROID: String = "android"
        const val DEF_PACKAGE_APP: String = BuildConfig.APPLICATION_ID

        const val DEFAULT_SYSTEM_UI_FLAG: Int =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

        const val NV_MENU: Int = 0
        const val NV_TIMELINE: Int = 1
        const val NV_HEADSET_SETTING: Int = 2
        const val NV_HEADSET_GUIDE: Int = 3
        const val NV_HEALING_MUSIC_GUIDE: Int = 3

        var brainEventListener:BrainEventListener? = null

        fun setOmnifitListener(listener: BrainEventListener) {
            brainEventListener = listener
        }
    }

    interface OnFocusListenable {
        fun onWindowFocusChanged(hasFocus: Boolean)
    }

    interface OnDrawerStateChangeListener {
        fun onStateChanged(owner: LifecycleOwner, state: Int)

        companion object {
            const val STATE_CLOSED: Int = 0
            const val STATE_OPENED: Int = 1
        }
    }

    interface OnPlaybackStateChangeListener {
        fun onPlaybackStateChanged(state: Int, curr: Int, duration: Int)
    }
}