package omnifit.brain.ceragemui.data

import com.squareup.moshi.Json

data class AudioContent(
    @Json(name = "musicContsSeq")   var sequence: Int = -1,
    @Json(name = "musicNm")         var title: String = "",
    @Json(name = "ord")             var order: Int = 0,
    @Json(name = "xpln")            var explanation: String = "",
    @Json(name = "musicGrpCd")      var groupSignature: String = "",
    @Json(name = "externalDndlUrl") var externalDownloadUrl: String = "",
    @Json(name = "introYn")         var introducable: Boolean = false,
    @Json(name = "imgFile")         var imageFile: ContentFileInfo? = null,
    @Json(name = "file")            var audioFile: ContentFileInfo? = null,
    @Json(name = "fileEn")          var audioFileEn: ContentFileInfo? = null
)