package omnifit.brain.ceragemui.data

import com.squareup.moshi.Json
import io.realm.annotations.Ignore

data class PlaybackContent(
    @Json(name = "playlistSeq")  var sequence: Int = -1,
    @Json(name = "titl")         var title: String = "",
    @Json(name = "xpln")         var explanation: String = "",
    @Json(name = "useTypeCd")    var utsSignature: String = "",
    @Json(name = "playlistOrd")  var order: Int = 0,
    @Json(name = "decisionCd")   var micSignature: String = "",
    @Json(name = "useSvcCd")     var svcSignature: String = "",
    @Json(name = "musicTypeCd")  var mucSignature: String = "",
    @Json(name = "msmtTypeCd")   var mstSignature: String = "",
    @Json(name = "musicConts")   var audioContentList: List<AudioContent>? = null,
    @Ignore                      var featuredSeq: Int = -1
)