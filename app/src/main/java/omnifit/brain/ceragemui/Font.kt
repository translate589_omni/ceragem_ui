package omnifit.brain.ceragemui

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan
import androidx.annotation.FontRes
import androidx.collection.SparseArrayCompat
import androidx.core.content.res.ResourcesCompat
import lt.neworld.spanner.SpanBuilder
import org.jetbrains.anko.doAsync
import java.util.*

object Font {
    private val typefaceCache: SparseArrayCompat<Typeface?> = SparseArrayCompat()

    fun init(c: Context) {
        doAsync {
            typefaceCache.put(R.font.noto_sans_kr_light, get(c, R.font.noto_sans_kr_light))
            typefaceCache.put(R.font.noto_sans_kr_regular, get(c, R.font.noto_sans_kr_regular))
            typefaceCache.put(R.font.noto_sans_kr_medium, get(c, R.font.noto_sans_kr_medium))
            typefaceCache.put(R.font.noto_sans_kr_bold, get(c, R.font.noto_sans_kr_bold))
            typefaceCache.put(R.font.lato_light, get(c, R.font.lato_light))
            typefaceCache.put(R.font.lato_regular, get(c, R.font.lato_regular))
            typefaceCache.put(R.font.lato_bold, get(c, R.font.lato_bold))
            typefaceCache.put(R.font.helvetica_neue_roman, get(c, R.font.helvetica_neue_roman))
            typefaceCache.put(R.font.helvetica_neue_bold, get(c, R.font.helvetica_neue_bold))
            typefaceCache.put(R.font.futura_bold, get(c, R.font.futura_bold))
            typefaceCache.put(R.font.jalnan, get(c, R.font.jalnan))
            typefaceCache.put(R.font.tium, get(c, R.font.tium))
        }
    }

    val notoSansKrLight: Typeface? by lazy { typefaceCache[R.font.noto_sans_kr_light] }
    val notoSansKrRegular: Typeface? by lazy { typefaceCache[R.font.noto_sans_kr_regular] }
    val notoSansKrMedium: Typeface? by lazy { typefaceCache[R.font.noto_sans_kr_medium] }
    val notoSansKrBold: Typeface? by lazy { typefaceCache[R.font.noto_sans_kr_bold] }
    val latoLight: Typeface? by lazy { typefaceCache[R.font.lato_light] }
    val latoRegular: Typeface? by lazy { typefaceCache[R.font.lato_regular] }
    val latoBold: Typeface? by lazy { typefaceCache[R.font.lato_bold] }
    val helveticaNeueRoman: Typeface? by lazy { typefaceCache[R.font.helvetica_neue_roman] }
    val helveticaNeueBold: Typeface? by lazy { typefaceCache[R.font.helvetica_neue_bold] }
    val futuraBold: Typeface? by lazy { typefaceCache[R.font.futura_bold] }
    val jalnan: Typeface? by lazy { typefaceCache[R.font.jalnan] }
    val tium: Typeface? by lazy { typefaceCache[R.font.tium]}

    fun lightByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> notoSansKrLight
            else                  -> latoLight
        }
    }

    fun regularByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> notoSansKrRegular
            else                  -> latoRegular
        }
    }

    fun mediumByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> notoSansKrMedium
            else                  -> latoRegular
        }
    }

    fun boldByLanguage(): Typeface? {
        return when (Locale.getDefault().language) {
            Locale.KOREA.language -> notoSansKrBold
            else                  -> latoBold
        }
    }

    private fun get(c: Context, @FontRes id: Int) = ResourcesCompat.getFont(c, id)

    class TypefaceSpan(private val typeface: Typeface?) : MetricAffectingSpan() {

        override fun updateDrawState(state: TextPaint) {
            apply(state)
        }

        override fun updateMeasureState(paint: TextPaint) {
            apply(paint)
        }

        private fun apply(paint: Paint) {
            val oldStyle = paint.typeface?.style ?: 0
            val fakeStyle = typeface?.style?.inv()?.and(oldStyle) ?: 0
            if (fakeStyle and Typeface.BOLD != 0) paint.isFakeBoldText = true
            if (fakeStyle and Typeface.ITALIC != 0) paint.textSkewX = -0.25f
            paint.typeface = typeface
        }
    }

    class CustomTypefaceSpan(private val typeface: Typeface?) : SpanBuilder {
        override fun build(): Any {
            return TypefaceSpan(typeface)
        }
    }
}