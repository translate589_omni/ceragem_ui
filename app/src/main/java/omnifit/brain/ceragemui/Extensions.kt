package omnifit.brain.ceragemui

import android.annotation.SuppressLint
import android.database.Observable
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.MotionEvent
import android.view.TouchDelegate
import android.view.View
import android.view.ViewManager
import android.widget.*
import androidx.annotation.AnimRes
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commitNow
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.afollestad.materialdialogs.MaterialDialog
import com.github.mikephil.charting.charts.LineChart
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.ceragemui.algorithm.ServiceCategory
import omnifit.brain.ceragemui.view.HeadsetStateVisualizer
import omnifit.sdk.omnifitbrain.ElectrodeState
import omnifit.sdk.omnifitbrain.LinkState
import omnifit.sdk.omnifitbrain.OmnifitBrain
import omnifit.sdk.common.isAvailable
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.resources
import org.jetbrains.anko.sp
import timber.log.Timber


fun UIFrame.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = supportFragmentManager.commitNow(allowStateLoss = true) {
    setCustomAnimations(enter, exit)
    replace(R.id.screen_container, screen, screen::class.java.simpleName)
}.run { true }

fun UIScreen.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = (requireActivity() as? UIFrame)?.screenTo(screen, enter, exit) ?: false

fun AnkoContext<Fragment>.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out

) = (owner as? UIScreen)?.screenTo(screen, enter, exit) ?: false

fun UIFrame.identifier(
    name: String,
    defType: String,
    defPackage: String
): Int = resources.getIdentifier(name, defType, defPackage)

fun AnkoContext<Fragment>.identifier(
    name: String,
    defType: String,
    defPackage: String
): Int = resources.getIdentifier(name, defType, defPackage)

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥ VIEW
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

fun ImageView.into(drawable: Drawable?) {
    setImageDrawable(drawable)
}

@SuppressLint("CheckResult")
fun View.bindElectrodeStateChangeObserver(owner: LifecycleOwner) {
    OmnifitBrain.observeElectrodeStateChange()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { state ->
                when (state) {
                    ElectrodeState.ATTACHED -> {
                        when (this) {
                            is HeadsetStateVisualizer -> start()
                            is CheckBox -> isChecked = true
                            else -> {
                            }
                        }
                    }
                    else                    -> {
                        when (this) {
                            is HeadsetStateVisualizer -> stop()
                            is CheckBox -> isChecked = false
                            else -> {
                            }
                        }
                    }
                }
            },
            { e -> Timber.e(e) }
        )
}

@SuppressLint("CheckResult")
fun View.bindBatteryLevelChangeObserver(owner: LifecycleOwner) {
    OmnifitBrain.observeBatteryLevelChange()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { level ->
                when (this) {
                    is ProgressBar -> progress = level
                    is TextView -> {
                        text = Spanner().apply {
                            val sp1 = sp(24f)
                            val sp2 = sp(17.6f)
                            val font = Font.CustomTypefaceSpan(Font.regularByLanguage())
                            val color = ResourcesCompat.getColor(resources, R.color.x_000000, null)

                            append("$level", Spans.sizeSP(sp1), Spans.custom(font), Spans.foreground(color))
                            append("%", Spans.sizeSP(sp2), Spans.custom(font), Spans.foreground(color))
                        }
                    }
                    else -> {
                    }
                }

            },
            { e -> Timber.e(e) }
        )
}

@SuppressLint("CheckResult")
fun View.bindLinkStateVisibleChangeObserver(owner: LifecycleOwner) {
    OmnifitBrain.observeLinkStateChange()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { state ->
                when(state) {
                    is LinkState.Linked -> true
                    else -> false
                }.let {
                    isVisible = it
                }
            },
            { e -> Timber.e(e) }
        )
}

@SuppressLint("CheckResult")
fun ImageButton.bindLinkStateChangeObserver(owner: LifecycleOwner) {
    OmnifitBrain.observeLinkStateChange()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { state ->
                setImageLevel(if (state is LinkState.Linked) 1 else 0)
            },
            { e -> Timber.e(e) }
        )
}

class CompositeTouchDelegate(view: View) : TouchDelegate(emptyRect, view) {

    private val delegates = ArrayList<TouchDelegate>()

    fun addDelegate(delegate: TouchDelegate) {
        delegates.add(delegate)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        var rst = false
        for (delegate in delegates) {
            event.setLocation(event.x, event.y)
            rst = delegate.onTouchEvent(event) || rst
        }
        return rst
    }

    companion object {
        private val emptyRect = Rect()
    }

}

fun <V : View> V.bindTouchDelegate(extraSpace: Int) {
    (parent as? View)?.let { p ->
        p.post {
            Rect().apply {
                this@bindTouchDelegate.getHitRect(this)
                left -= extraSpace
                top -= extraSpace
                right += extraSpace
                bottom += extraSpace
            }.let { r ->
                p.touchDelegate?.let { composer ->
                    (composer as CompositeTouchDelegate).addDelegate(TouchDelegate(r, this@bindTouchDelegate))
                } ?: run {
                    p.touchDelegate = CompositeTouchDelegate(this).also { composer ->
                        composer.addDelegate(TouchDelegate(r, this@bindTouchDelegate))
                    }
                }
            }
        }
    }
}


fun ImageView.blurInto(sequence: Int) {
    //setImageDrawable(PlaybackContentHelper.healingMusicBlurImageFrom(sequence))
}

fun View.moveToExternalLink(
    linkUrl: String,
    startEnterAnim: Int = R.anim.slide_enter_right,
    startExitAnim: Int = R.anim.slide_hold,
    exitEnterAnim: Int = R.anim.slide_hold,
    exitExitAnim: Int = R.anim.slide_exit_right
) {
    CustomTabsIntent.Builder()
        .setShowTitle(true)
        .enableUrlBarHiding()
        .setStartAnimations(context, startEnterAnim, startExitAnim)
        .setExitAnimations(context, exitEnterAnim, exitExitAnim)
        .build()
        .launchUrl(context, Uri.parse(linkUrl))
}

inline fun ViewManager.lineChart(theme: Int = 0, init: LineChart.() -> Unit): LineChart {
    return ankoView({ LineChart(it) }, theme, init)
}

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥ ETC
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

fun List<Double>.ignoreNegative(): List<Double> = filter { it.isAvailable() }
fun List<Double>.ignoreNegativeAverage(): Double = ignoreNegative().average()

fun MaterialDialog.hideNavigation() {
    window?.decorView?.systemUiVisibility = UIFrame.DEFAULT_SYSTEM_UI_FLAG
}

inline fun Boolean.whenTrue(block: () -> Unit): Boolean {
    if (this) block()
    return this
}