package omnifit.brain.ceragemui.helper

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.net.Uri
import android.util.SparseArray
import com.squareup.moshi.Types
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.ceragemui.BuildConfig
import omnifit.brain.ceragemui.SIGNATURE_MGC_HEALING_MUSIC
import omnifit.brain.ceragemui.data.AudioContent
import omnifit.brain.ceragemui.data.ContentFileInfo
import omnifit.brain.ceragemui.data.PlaybackContent
import omnifit.sdk.audioplayer.AudioPlayer
import omnifit.sdk.common.json.JsonConverters
import omnifit.sdk.common.secondsToMillis
import org.apache.commons.io.FilenameUtils
import org.jetbrains.anko.collections.asSequence
import timber.log.Timber
import kotlin.math.roundToInt
import kotlin.properties.Delegates

object ContentHelper {

    @Suppress("CheckResult")
    fun requireBaseData(
        context: Context,
        on: (Float) -> Unit
    ) {
        on(35.0f)

        context.assets.open("json/playback.json").use { stream ->
            ByteArray(stream.available()).also { buffer ->
                stream.read(buffer)
            }.let { buffer ->
                ContentDataHelper.playbackList = JsonConverters.moshi()
                    .adapter<List<PlaybackContent>>(
                        Types.newParameterizedType(
                            List::class.java,
                            PlaybackContent::class.java
                        )
                    )
                    .fromJson(String(buffer))
            }
        }
        on(100.0f)
    }

    @Suppress("CheckResult")
    fun initLoad(
        onLoad: (Int) -> Unit
    ) {
        clearCaches()
        // 명상 힐링
        ContentDataHelper.findPlaybackListAll()
            .sumBy { pc ->
                pc.audioContentList?.count() ?: 0
            }
            .let { totalCount ->
                ContentDataHelper.findPlaybackListAllAsFlowable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .flatMapIterable { pc ->
                        pc.getNonIntroContentFilteredIterable()
                    }
                    .map { (pc, ac) ->
                        when (ac.groupSignature) {
                            SIGNATURE_MGC_HEALING_MUSIC -> {
                                HEALING_MUSIC_PLAYLIST_CACHE.put(
                                    ac.sequence,
                                    AudioPlayer.AudioSource(
                                        sequence = ac.sequence,
                                        albumTitle = pc.title,
                                        title = ac.title,
                                        duration = ac.getDuration(),
                                        explanation = ac.explanation,
                                        source = ac.getAudioSourceUri(),
                                        order = ac.order
                                    )
                                )
                            }
                            else -> {
                            }
                        }
                    }
                    .scan(0) { count, _ ->
                        count.inc()
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { count ->
                            count.toFloat().div(totalCount.toFloat()).times(101.0f).roundToInt().let(onLoad)
                        },
                        { e -> Timber.e(e) },
                        { onLoad(102) }
                    )
            }
    }

    fun healingMusicPlaylist(): List<AudioPlayer.AudioSource> {
        return HEALING_MUSIC_PLAYLIST_CACHE.asSequence().toList()
    }

    private val HEALING_MUSIC_PLAYLIST_CACHE: SparseArray<AudioPlayer.AudioSource> = SparseArray()

    private var specificKey: Int by Delegates.observable(0) { _, o, n ->
        if (o != n) {
            meditationSourceOrder = 0
        }
    }

    private var meditationSourceOrder: Int = 0

    /**
     * 같은 카테고리 그룹에 속하면서 앨범이 나뉘어져 공통 키를 구할때 정렬기준 1번에 맞춰 키를 고정
     * ex: sequence:94, order:1 -> 94 - (1 - 1) = 94
     *     sequence:95, order:2 -> 95 - (2 - 1) = 94
     */
    private fun specificKeyFrom(pc: PlaybackContent): Int {
        return pc.sequence - (pc.order - 1)
    }

    private fun PlaybackContent.getNonIntroContentFilteredIterable(): Iterable<Pair<PlaybackContent, AudioContent>> {
        return audioContentList
            ?.filter { ac ->
                !ac.introducable
            }
            ?.map { ac ->
                Pair(this, ac)
            }
            ?: mutableListOf()
    }

    private fun AudioContent.getAudioSourceUri(): Uri {
        return audioFile?.getContentSourceUri() ?: Uri.EMPTY
    }

    private fun PlaybackContent.getIntroContentSource(context: Context): AudioPlayer.AudioSource? {
        return try {
            audioContentList
                ?.first { ac ->
                    ac.introducable
                }
                ?.let { ac ->
                    AudioPlayer.AudioSource(
                        sequence = ac.sequence,
                        albumTitle = title,
                        title = ac.title,
                        explanation = ac.explanation,
                        source = ac.getAudioSourceUri(),
                        order = ac.order
                    )
                }
        } catch (e: NoSuchElementException) {
            null
        }
    }

    private fun AudioContent.getDuration(): Long {
        return audioFile?.duration?.secondsToMillis() ?: 0L
    }

    private fun clearCaches() {
        HEALING_MUSIC_PLAYLIST_CACHE.clear()
    }
}

fun ContentFileInfo.getContentSourceToFileDescriptor(context: Context): AssetFileDescriptor {
    return context.assets.openFd("${assetDirectoryName(downloadUrl)}/${FilenameUtils.getName(downloadUrl)}")
}

fun ContentFileInfo.getContentSourceUri(): Uri {
    val fileName = assetDirectoryName(downloadUrl) + "_" + name
    return Uri.parse("android.resource://${BuildConfig.APPLICATION_ID}/raw/$fileName")
}

private fun assetDirectoryName(filename: String): String {
    return when (FilenameUtils.getExtension(filename)) {
        "mp3" -> "audio"
        "mp4" -> "video"
        else -> "image"
    }
}