package omnifit.brain.ceragemui.helper

import omnifit.brain.ceragemui.model.ELECTROENCEPHALOGRAPHY
import omnifit.brain.ceragemui.model.Electroencephalography
import omnifit.brain.ceragemui.model.Healing
import org.jetbrains.anko.doAsync

object ServiceResultHelper {

    fun createHealingMusicResult(
        elapsedTime: Int,
        result: List<ELECTROENCEPHALOGRAPHY>,
        callback: (Healing) -> Unit
    ) {
        doAsync {
            Electroencephalography.create(elapsedTime, result).let { e ->
                Healing.create(elapsedTime, e) { h ->
                    callback(h)
                }
            }
        }
    }

    const val ARGUMENTS_START_HEALING: String = "START_HEALING"
    const val ARGUMENTS_RESULT_HEALING: String = "RESULT_HEALING"
}