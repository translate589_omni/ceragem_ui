package omnifit.brain.ceragemui.helper

import io.reactivex.Flowable
import io.reactivex.rxkotlin.toFlowable
import omnifit.brain.ceragemui.SIGNATURE_UTS_HEALING_MUSIC
import omnifit.brain.ceragemui.SIGNATURE_UTS_MEDITATION
import omnifit.brain.ceragemui.SIGNATURE_UTS_MEDITATION_MBSR
import omnifit.brain.ceragemui.SIGNATURE_UTS_MEDITATION_TRAINING
import omnifit.brain.ceragemui.data.*

object ContentDataHelper {

    var playbackList: List<PlaybackContent>? = listOf()

    fun findPlaybackListAll(): List<PlaybackContent> {
        return playbackList!!
            .filter { pc ->
                pc.utsSignature in arrayOf(
                    SIGNATURE_UTS_HEALING_MUSIC
                )
            }
            .sortedBy { pc ->
                pc.order
            }
    }

    fun findPlaybackListAllAsFlowable(): Flowable<PlaybackContent> {
        return playbackList!!
            .sortedBy { pc ->
                pc.order
            }
            .toFlowable()
    }
}