package omnifit.brain.ceragemui.common

import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import omnifit.sdk.common.BaseObservableProperty

fun <T : TextView> T.bindText(bindable: BaseObservableProperty<String>) {
    text = bindable.value
    bindable.addValueChangeListener { text = it }
}

fun <T : TextView> T.bindVisible(bindable: BaseObservableProperty<Boolean>) {
    isVisible = bindable.value
    bindable.addValueChangeListener { isVisible = it }
}

fun <T : ImageView> T.bindImageLevel(bindable: BaseObservableProperty<Int>) {
    setImageLevel(bindable.value)
    bindable.addValueChangeListener { setImageLevel(it) }
}