package omnifit.brain.ceragemui.model

import omnifit.sdk.common.secondsToMillis
import java.util.*

open class Healing(
    open var id: String = "",
    open var begin: Date = Date(),
    open var end: Date = Date(),
    open var indicatorValues: String = "[]",
    open var elapsedTime: Long = 0,
    open var duration: Long = 0
) {

    companion object {
        const val ID_PREFIX: String = "H"

        fun create(
            _elapsedTime: Int,
            electroencephalography: Electroencephalography,
            cb: (Healing) -> Unit = {}
        ) = Healing().apply {
            id = "$ID_PREFIX${electroencephalography.id}"
            begin = electroencephalography.begin
            end = electroencephalography.end
            elapsedTime = _elapsedTime.secondsToMillis()
            duration = _elapsedTime.toLong()
            indicatorValues = electroencephalography.meanRelaxationIndicatorValues

            cb(this)
        }
    }
}