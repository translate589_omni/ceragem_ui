package omnifit.brain.ceragemui.model

import android.util.SparseArray
import omnifit.sdk.omnifitbrain.*
import omnifit.sdk.common.DTP_YYYYMMDDHHMMSS
import omnifit.sdk.common.algorithm.BrainBalanceIndicator
import omnifit.sdk.common.columnAverage
import omnifit.sdk.common.isAvailable
import omnifit.sdk.common.json.fromJson
import omnifit.sdk.common.json.toJson
import omnifit.sdk.ocwh20.*
import org.joda.time.DateTime
import timber.log.Timber
import java.util.*

open class Electroencephalography(
    open var id: String = "", // yyyyMMddHHmmss
    open var begin: Date = Date(),
    open var end: Date = Date(),
    open var targetSignature: String = "", // 측정데이터의 대상 ID
    open var leftThetaIndicatorValues: String = "[]",
    open var meanLeftThetaIndicatorValue: Double = 0.0,
    open var rightThetaIndicatorValues: String = "[]",
    open var meanRightThetaIndicatorValue: Double = 0.0,
    open var meanThetaIndicatorValues: String = "[]",
    open var meanThetaIndicatorValue: Double = 0.0,
    open var leftAlphaIndicatorValues: String = "[]",
    open var meanLeftAlphaIndicatorValue: Double = 0.0,
    open var rightAlphaIndicatorValues: String = "[]",
    open var meanRightAlphaIndicatorValue: Double = 0.0,
    open var meanAlphaIndicatorValues: String = "[]",
    open var meanAlphaIndicatorValue: Double = 0.0,
    open var leftLowBetaIndicatorValues: String = "[]",
    open var meanLeftLowBetaIndicatorValue: Double = 0.0,
    open var rightLowBetaIndicatorValues: String = "[]",
    open var meanRightLowBetaIndicatorValue: Double = 0.0,
    open var meanLowBetaIndicatorValues: String = "[]",
    open var meanLowBetaIndicatorValue: Double = 0.0,
    open var leftMiddleBetaIndicatorValues: String = "[]",
    open var meanLeftMiddleBetaIndicatorValue: Double = 0.0,
    open var rightMiddleBetaIndicatorValues: String = "[]",
    open var meanRightMiddleBetaIndicatorValue: Double = 0.0,
    open var meanMiddleBetaIndicatorValues: String = "[]",
    open var meanMiddleBetaIndicatorValue: Double = 0.0,
    open var leftHighBetaIndicatorValues: String = "[]",
    open var meanLeftHighBetaIndicatorValue: Double = 0.0,
    open var rightHighBetaIndicatorValues: String = "[]",
    open var meanRightHighBetaIndicatorValue: Double = 0.0,
    open var meanHighBetaIndicatorValues: String = "[]",
    open var meanHighBetaIndicatorValue: Double = 0.0,
    open var leftGammaIndicatorValues: String = "[]",
    open var meanLeftGammaIndicatorValue: Double = 0.0,
    open var rightGammaIndicatorValues: String = "[]",
    open var meanRightGammaIndicatorValue: Double = 0.0,
    open var meanGammaIndicatorValues: String = "[]",
    open var meanGammaIndicatorValue: Double = 0.0,
    open var concentrationIndicatorValues: String = "[]",
    open var meanConcentrationIndicatorValue: Double = 0.0,
    open var leftRelaxationIndicatorValues: String = "[]",
    open var meanLeftRelaxationIndicatorValue: Double = 0.0,
    open var rightRelaxationIndicatorValues: String = "[]",
    open var meanRightRelaxationIndicatorValue: Double = 0.0,
    open var meanRelaxationIndicatorValues: String = "[]",
    open var meanRelaxationIndicatorValue: Double = 0.0,
    open var balanceIndicatorValues: String = "[]",
    open var meanBalanceIndicatorValue: Double = 0.0,
    open var leftSef90Hzs: String = "[]",
    open var meanLeftSef90Hz: Double = 0.0,
    open var rightSef90Hzs: String = "[]",
    open var meanRightSef90Hz: Double = 0.0,
    open var meanSef90Hzs: String = "[]",
    open var meanSef90HzIndicatorValues: String = "[]",
    open var meanSef90Hz: Double = 0.0,
    open var meanLeftPowerSpectrum: String = "[]",
    open var meanRightPowerSpectrum: String = "[]",
    open var meanPowerSpectrum: String = "[]",
    open var meanLeftRhythmDistribution: String = "[]",
    open var meanRightRhythmDistribution: String = "[]",
    open var meanRhythmDistribution: String = "[]"
) {
    companion object {
        fun create(
            elapsedTime: Int,
            electroencephalographies: List<ELECTROENCEPHALOGRAPHY>
        ) = Electroencephalography().apply {
            var timeTemp = System.currentTimeMillis()

            DateTime.now().let { end ->
                end.minusSeconds(elapsedTime).let { begin ->
                    this.id = begin.toString(DTP_YYYYMMDDHHMMSS)
                    this.begin = begin.toDate()
                    this.end = end.toDate()
                }
            }
            with(SparseArray<DoubleArray>()) {
                fun ELECTROENCEPHALOGRAPHY.byKey(key: Int): Double {
                    return when (key) {
                        RESULT_ITEM_L_THETA -> leftThetaIndicatorValue
                        RESULT_ITEM_R_THETA -> rightThetaIndicatorValue
                        RESULT_ITEM_L_ALPHA -> leftAlphaIndicatorValue
                        RESULT_ITEM_R_ALPHA -> rightAlphaIndicatorValue
                        RESULT_ITEM_L_LBETA -> leftLowBetaIndicatorValue
                        RESULT_ITEM_R_LBETA -> rightLowBetaIndicatorValue
                        RESULT_ITEM_L_MBETA -> leftMiddleBetaIndicatorValue
                        RESULT_ITEM_R_MBETA -> rightMiddleBetaIndicatorValue
                        RESULT_ITEM_L_HBETA -> leftHighBetaIndicatorValue
                        RESULT_ITEM_R_HBETA -> rightHighBetaIndicatorValue
                        RESULT_ITEM_L_GAMMA -> leftGammaIndicatorValue
                        RESULT_ITEM_R_GAMMA -> rightGammaIndicatorValue
                        RESULT_ITEM_CONCENTRATION -> concentrationIndicatorValue
                        RESULT_ITEM_L_RELAXATION -> leftRelaxationIndicatorValue
                        RESULT_ITEM_R_RELAXATION -> rightRelaxationIndicatorValue
                        RESULT_ITEM_BALANCE -> balanceIndicatorValue
                        RESULT_ITEM_L_SEF90HZ -> leftSef90Hz
                        RESULT_ITEM_R_SEF90HZ -> rightSef90Hz
                        else -> 0.0
                    }
                }

                fun SparseArray<DoubleArray>.setEegData(
                    key: Int,
                    index: Int,
                    eeg: ELECTROENCEPHALOGRAPHY
                ) {
                    get(key)?.set(
                        index,
                        eeg.byKey(key)
                    ) ?: put(
                        key,
                        DoubleArray(electroencephalographies.size).apply {
                            set(index, eeg.byKey(key))
                        }
                    )
                }

                fun SparseArray<DoubleArray>.getPair(firstKey:Int, secondKey:Int) = Pair(get(firstKey), get(secondKey))

                timeTemp = System.currentTimeMillis()
                electroencephalographies.forEachIndexed { index, electroencephalography ->
                    setEegData(RESULT_ITEM_L_THETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_THETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_ALPHA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_ALPHA, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_LBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_LBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_MBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_MBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_HBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_HBETA, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_GAMMA, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_GAMMA, index, electroencephalography)
                    setEegData(RESULT_ITEM_CONCENTRATION, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_RELAXATION, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_RELAXATION, index, electroencephalography)
                    setEegData(RESULT_ITEM_BALANCE, index, electroencephalography)
                    setEegData(RESULT_ITEM_L_SEF90HZ, index, electroencephalography)
                    setEegData(RESULT_ITEM_R_SEF90HZ, index, electroencephalography)
                }

                getPair(RESULT_ITEM_L_THETA,RESULT_ITEM_R_THETA).run {
                    leftThetaIndicatorValues = first.toJson()
                    meanLeftThetaIndicatorValue = first.ignoreNegativeAverage()

                    rightThetaIndicatorValues = second.toJson()
                    meanRightThetaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanThetaIndicatorValues = it.toJson()
                        meanThetaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_ALPHA,RESULT_ITEM_R_ALPHA).run {
                    leftAlphaIndicatorValues = first.toJson()
                    meanLeftAlphaIndicatorValue = first.ignoreNegativeAverage()

                    rightAlphaIndicatorValues = second.toJson()
                    meanRightAlphaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanAlphaIndicatorValues = it.toJson()
                        meanAlphaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_LBETA,RESULT_ITEM_R_LBETA).run {
                    leftLowBetaIndicatorValues = first.toJson()
                    meanLeftLowBetaIndicatorValue = first.ignoreNegativeAverage()

                    rightLowBetaIndicatorValues = second.toJson()
                    meanRightLowBetaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanLowBetaIndicatorValues = it.toJson()
                        meanLowBetaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_MBETA,RESULT_ITEM_R_MBETA).run {
                    leftMiddleBetaIndicatorValues = first.toJson()
                    meanLeftMiddleBetaIndicatorValue = first.ignoreNegativeAverage()

                    rightMiddleBetaIndicatorValues = second.toJson()
                    meanRightMiddleBetaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanMiddleBetaIndicatorValues = it.toJson()
                        meanMiddleBetaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_HBETA,RESULT_ITEM_R_HBETA).run {
                    leftHighBetaIndicatorValues = first.toJson()
                    meanLeftHighBetaIndicatorValue = first.ignoreNegativeAverage()

                    rightHighBetaIndicatorValues = second.toJson()
                    meanRightHighBetaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanHighBetaIndicatorValues = it.toJson()
                        meanHighBetaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_GAMMA,RESULT_ITEM_R_GAMMA).run {
                    leftGammaIndicatorValues = first.toJson()
                    meanLeftGammaIndicatorValue = first.ignoreNegativeAverage()

                    rightGammaIndicatorValues = second.toJson()
                    meanRightGammaIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanGammaIndicatorValues = it.toJson()
                        meanGammaIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_RELAXATION,RESULT_ITEM_R_RELAXATION).run {
                    leftRelaxationIndicatorValues = first.toJson()
                    meanLeftRelaxationIndicatorValue = first.ignoreNegativeAverage()

                    rightRelaxationIndicatorValues = second.toJson()
                    meanRightRelaxationIndicatorValue = second.ignoreNegativeAverage()

                    arrayOf(first,second).columnAverage().let {
                        meanRelaxationIndicatorValues = it.toJson()
                        meanRelaxationIndicatorValue = it.ignoreNegativeAverage()
                    }
                }

                getPair(RESULT_ITEM_L_SEF90HZ,RESULT_ITEM_R_SEF90HZ).run {
                    leftSef90Hzs = first.toJson()
                    rightSef90Hzs = second.toJson()
                }

                with(get(RESULT_ITEM_CONCENTRATION,doubleArrayOf())) {
                    concentrationIndicatorValues = toJson()
                    meanConcentrationIndicatorValue = ignoreNegativeAverage()
                }

                with(get(RESULT_ITEM_BALANCE,doubleArrayOf())) {
                    balanceIndicatorValues = toJson()
                    meanBalanceIndicatorValue = ignoreNegativeAverage()
                }
            }

            // 신규 좌우뇌균형도 서비스 알고리즘으로 교체
            electroencephalographies.map {
                arrayOf(
                    it.leftPowerSpectrum.copyOfRange(9, 62),
                    it.rightPowerSpectrum.copyOfRange(9, 62)
                ).columnAverage().average().let { avg ->
                    if (avg > 0.0) BrainBalanceIndicator.calc11piValueFromPowerSpectrum(
                        it.leftPowerSpectrum,
                        it.rightPowerSpectrum
                    )
                    else avg
                }
            }.let { `11pivs` ->
                balanceIndicatorValues = `11pivs`.toDoubleArray().toJson()
                meanBalanceIndicatorValue = `11pivs`.toDoubleArray().ignoreNegativeAverage()
            }

            electroencephalographies.filter { it.leftPowerSpectrum.average() > 0.0 }
                .map { it.leftPowerSpectrum }
                .toTypedArray()
                .columnAverage()
                .let { _meanLeftPowerSpectrum ->
                    meanLeftPowerSpectrum = _meanLeftPowerSpectrum.toJson()
                    meanLeftSef90Hz = _meanLeftPowerSpectrum.toSef90Hz(true)
                    meanLeftRhythmDistribution =
                        _meanLeftPowerSpectrum.toRhythmDistribution().toJson()
                }

            electroencephalographies.filter { it.rightPowerSpectrum.average() > 0.0 }
                .map { it.rightPowerSpectrum }
                .toTypedArray()
                .columnAverage()
                .let { _meanRightPowerSpectrum ->
                    meanRightPowerSpectrum = _meanRightPowerSpectrum.toJson()
                    meanRightSef90Hz = _meanRightPowerSpectrum.toSef90Hz(true)
                    meanRightRhythmDistribution =
                        _meanRightPowerSpectrum.toRhythmDistribution().toJson()
                }

            electroencephalographies.filter { it.leftPowerSpectrum.average() > 0.0 && it.rightPowerSpectrum.average() > 0.0 }
                .map {
                    arrayOf(
                        it.leftPowerSpectrum,
                        it.rightPowerSpectrum
                    ).columnAverage()
                }
                .toTypedArray()
                .columnAverage()
                .let { _meanPowerSpectrum ->
                    meanPowerSpectrum = _meanPowerSpectrum.toJson()
                    meanSef90Hz = _meanPowerSpectrum.toSef90Hz(true)
                    meanRhythmDistribution = _meanPowerSpectrum.toRhythmDistribution().toJson()
                }

            electroencephalographies.map {
                arrayOf(
                    it.leftPowerSpectrum,
                    it.rightPowerSpectrum
                ).columnAverage()
            }.let {
                meanSef90Hzs = it.map { meanPowerSpectrum ->
                    meanPowerSpectrum.average().let { avg ->
                        if (avg > 0.0) meanPowerSpectrum.toSef90Hz(true)
                        else avg
                    }
                }.toDoubleArray().toJson()
                meanSef90HzIndicatorValues = it.map { meanPowerSpectrum ->
                    meanPowerSpectrum.average().let { avg ->
                        if (avg > 0.0) transformSef90HzTo11piValue(meanPowerSpectrum.toSef90Hz(true))
                        else avg
                    }
                }.toDoubleArray().toJson()
            }
        }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private fun DoubleArray.ignoreNegativeAverage(): Double {
        return filter { it.isAvailable() }.average()
    }
}

typealias ELECTROENCEPHALOGRAPHY = omnifit.sdk.omnifitbrain.Electroencephalography