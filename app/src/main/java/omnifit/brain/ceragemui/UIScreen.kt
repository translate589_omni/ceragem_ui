package omnifit.brain.ceragemui

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import omnifit.brain.ceragemui.algorithm.ServiceCategory
import omnifit.brain.ceragemui.helper.ContentHelper
import omnifit.sdk.audioplayer.AudioPlayer
import omnifit.sdk.omnifitbrain.OmnifitBrain
import kotlin.system.exitProcess

abstract class UIScreen : Fragment(),
    UIFrame.OnFocusListenable,
    UIFrame.OnDrawerStateChangeListener,
    UIFrame.OnPlaybackStateChangeListener  {
    var isRootScreen: Boolean = false
    var shouldChangeStatusBarTintToDark: Boolean = false

    private var screenFrame: UIFrame? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        changeBrightnessOfStatusBarIfNeed()
        screenFrame = context as? UIFrame
        screenFrame?.requireDensity()
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (nextAnim == 0) {
            onScreenTransitionEnd(this, enter)
            return super.onCreateAnimation(transit, enter, nextAnim)
        }
        return AnimationUtils.loadAnimation(requireContext(), nextAnim).run {
            view?.let { v ->
                v.setLayerType(View.LAYER_TYPE_HARDWARE, null)
                setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        if (nextAnim == R.anim.slide_enter_right) {
                            ViewCompat.setTranslationZ(v, 1.0f)
                        }
                        onScreenTransitionStart(this@UIScreen, enter)
                    }

                    override fun onAnimationRepeat(animation: Animation) {
                    }

                    override fun onAnimationEnd(animation: Animation) {
                        v.setLayerType(View.LAYER_TYPE_NONE, null)
                        if (nextAnim == R.anim.slide_enter_right) {
                            Handler(Looper.getMainLooper()).post {
                                ViewCompat.setTranslationZ(v, 0.0f)
                            }
                        }
                        onScreenTransitionEnd(this@UIScreen, enter)
                    }
                })
            }
            AnimationSet(true).apply { addAnimation(this@run) }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            changeBrightnessOfStatusBarIfNeed()
        }
    }

    private fun changeBrightnessOfStatusBarIfNeed() {
        (requireActivity() as? UIFrame)?.let { frame ->
            if (shouldChangeStatusBarTintToDark) frame.changeToDarkStatusBar()
            else frame.changeToLightStatusBar()
        }
    }

    open fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    open fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        if (isEnter) {
            if (isQuickOpened()) closeQuick()
        }
    }

    override fun onResume() {
        super.onResume()
        onUserLeave = false
    }

    @Volatile
    var onUserLeave: Boolean = false

    open fun onUserLeaveHint() {
        onUserLeave = true
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    open fun onHomePressed() {
    }

    open fun onRecentAppsPressed() {
    }

    override fun onStateChanged(owner: LifecycleOwner, state: Int) {
    }

    override fun onPlaybackStateChanged(status: Int, curr: Int, duration: Int) {
    }

    /**
     * 서비스 카테고리 지정자
     */
    var currentServiceCategory: ServiceCategory
        set(value) {
            screenFrame?.currentServiceCategory = value
        }
        get() = screenFrame?.currentServiceCategory ?: ServiceCategory.NORMAL

    fun performBackPressed() {
        screenFrame?.onBackPressed()
    }

    protected fun appFinish() : Boolean{
        OmnifitBrain.unlink(requireContext())

        //alert(requireContext(), R.string.notifications_020)

        requireActivity().run {
            (application as? CeragemApplication)?.close()
            finishAndRemoveTask()
        }

        return false
    }

    fun hideSoftKeyboard() {
        screenFrame?.hideSoftKeyboard()
    }

    val frameWidth: Int
        get() = screenFrame?.width ?: 0

    val frameHeight: Int
        get() = screenFrame?.height ?: 0

    val frameDpi: Int
        get() = screenFrame?.dpi ?: 0

    fun alert(
            context: Context,
            messageRes: Int,
            colorRes: Int = R.color.x_000000_op60,
            gravity: Int = Gravity.BOTTOM
    ) = screenFrame?.alert(context, messageRes, colorRes, gravity)

    fun alert(
            context: Context,
            message: String,
            colorRes: Int = R.color.x_000000_op60,
            gravity: Int = Gravity.BOTTOM
    ) = screenFrame?.alert(context, message, colorRes, gravity)

    protected val fadeIn: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
    }

    protected val fadeOut: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
    }

    fun isQuickOpened(): Boolean {
        return screenFrame?.isQuickOpened() ?: false
    }

    fun closeQuick() {
        screenFrame?.closeQuick()
    }

    fun isHeadsetSettingOpened(): Boolean {
        return screenFrame?.isHeadsetSettingOpened() ?: false
    }

    fun openHeadsetSetting(context: Context) {
        screenFrame?.openHeadsetSetting(context)
    }

    fun openHeadsetGuide(context: Context) {
        screenFrame?.openHeadsetGuide(context)
    }

    fun openHealingMusicGuide(context: Context) {
        screenFrame?.openHealingMusicGuide(context)
    }

    val playlist: MutableList<AudioPlayer.AudioSource>
        get() = ContentHelper.healingMusicPlaylist().toMutableList()

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    fun requireHeadsetState(): Boolean {
        return requireHeadsetLinkState() && requireHeadsetElectrodeState()
    }

    fun requireHeadsetLinkState(): Boolean {
        return OmnifitBrain.isLinked().also { isLinked ->
            if (!isLinked) {
                openHeadsetSetting(requireContext())
            }
        }
    }

    fun requireHeadsetElectrodeState(
        messageRes:Int = R.string.notifications_040,
        gravity: Int = Gravity.CENTER
    ): Boolean = OmnifitBrain.isElectrodeAttached().also { isAttached ->
        if (!isAttached) {
            alert(requireContext(), messageRes, gravity = gravity)
        }
    }
}