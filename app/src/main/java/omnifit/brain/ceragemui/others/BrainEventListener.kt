package omnifit.brain.ceragemui.others

interface BrainEventListener {
    fun onMovePurchaseScreen()
    fun onClose()
}