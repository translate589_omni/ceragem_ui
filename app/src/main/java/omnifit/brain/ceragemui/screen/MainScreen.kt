package omnifit.brain.ceragemui.screen

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import androidx.core.view.setPadding
import androidx.lifecycle.Lifecycle
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.tedpark.tedpermission.rx2.TedRx2Permission
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.brain.ceragemui.*
import omnifit.brain.ceragemui.R
import omnifit.brain.ceragemui.algorithm.ServiceCategory
import omnifit.brain.ceragemui.component.BasicPopComponent
import omnifit.brain.ceragemui.component.PermissionGuideComponent
import omnifit.brain.ceragemui.helper.ContentHelper
import omnifit.brain.ceragemui.helper.ServiceResultHelper
import omnifit.brain.ceragemui.model.Healing
import omnifit.brain.ceragemui.view.NonSpacingTextView
import omnifit.brain.ceragemui.view.headsetStatusVisualizer
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import omnifit.sdk.audioplayer.AudioPlayer
import omnifit.sdk.bluetooth.doseNothing
import omnifit.sdk.common.*
import omnifit.sdk.common.json.toJson
import omnifit.sdk.omnifitbrain.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.dip
import org.jetbrains.anko.support.v4.runOnUiThread
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates
import kotlin.random.Random

class MainScreen : UIScreen() {
    init {
        isRootScreen = true
    }

    private val isStartHealing by lazy {
        arguments?.getBoolean(ServiceResultHelper.ARGUMENTS_START_HEALING, false) ?: false
    }

    private val currentSoundType: Int by lazy {
        // 재생 할 카테고리 코드
        0
    }

    private var lazyPlayAudio: (() -> Unit)? = null

    private var selectedPosition: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            if (isAttached) playMusic(playlist[n])
            else lazyPlayAudio = { playMusic(playlist[n]) }
        }
    }

    private var isAttached: Boolean = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        isAttached = true
        lazyPlayAudio?.invoke()
    }

    override fun onDestroy() {
        stopMusic()
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
        isAttached = false
    }

    private var blurBg: ImageView? = null
    private var playingTimeView: NonSpacingTextView? = null
    private var titleTextView: TextView? = null
    private var durationTime: Int = 0
    private var analyzingView: View? = null

    // Translate589 여기에 힐링 뮤직 재생을 합친다
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = UI {
        constraintLayout {
            // 메뉴
            backgroundResource = R.drawable.img_bg_main

            blurBg = imageView {
                id = R.id.screen_widget_id_11
                scaleType = ImageView.ScaleType.CENTER_CROP
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }

            imageButton(R.drawable.selector_back) {
                id = R.id.screen_widget_id_01
                scaleType = ImageView.ScaleType.FIT_CENTER
                setPadding(dip(10.0f))

                onDebounceClick {
                    appFinish()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(19.3f)
                topToTop = PARENT_ID
                topMargin = dip(42.0f)
            }

            // 로고
            textView(R.string.main_screen_label_01) {
                id = R.id.screen_widget_id_02
                onDebounceClick {
                    openHealingMusicGuide(context)
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
                horizontalBias = 0.5f
            }

            // 헤드셋 설정
            imageButton(R.drawable.selector_headset_btn) {
                id = R.id.screen_widget_id_03
                scaleType = ImageView.ScaleType.CENTER
                onDebounceClick {
                    openHeadsetSetting(context)
                }
            }.lparams(dip(24.0f), dip(22.0f)) {
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                marginEnd = dip(17.5f)
                bottomToBottom = R.id.screen_widget_id_01
            }

            // 헤드셋 비주얼라이저
            headsetStatusVisualizer(icon = R.drawable.ic_brain) {
                id = R.id.screen_widget_id_04
                bindElectrodeStateChangeObserver(owner)
            }.lparams(dip(0), wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(24.8f)
                topToBottom = R.id.screen_widget_id_02
                topMargin = dip(17.5f)
                endToEnd = PARENT_ID
            }

            nonSpacingTextView(R.string.main_screen_label_02) {
                id = R.id.screen_widget_id_09
                textColorResource = R.color.x_ffffff
                textSize = 16.5f
                letterSpacing = -0.01f
                setLineSpacing(-13.5f, 0f)
                typeface = Font.boldByLanguage()
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_04
                endToEnd = PARENT_ID
                topMargin = dip(22.5f)
            }

            imageButton(R.drawable.selector_infor_btn) {
                id = R.id.screen_widget_id_10
                onDebounceClick {
                    openHeadsetGuide(context)
                }
            }.lparams(dip(21), dip(21)) {
                topToTop = R.id.screen_widget_id_09
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_09
                marginEnd = dip(20)
            }

            titleTextView = textView(R.string.main_screen_label_03) {
                id = R.id.screen_widget_id_11
                textSize = 12.5f
                letterSpacing = -0.03f
                setLineSpacing(6.5f, 0f)
                textColorResource = R.color.x_ffffff_op50
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_09
                endToEnd = PARENT_ID
                topMargin = dip(10)
            }

            imageView(R.drawable.selector_play) {
                id = R.id.screen_widget_id_12
                onDebounceClick {
                    when (isMeasureStarted) {
                        true -> {
                            if (isResultsCanBeViewed()) {
                                isActivated = false
                                OmnifitBrain.stopMeasure()
                            } else {
                                showNotMeasuredDialog {
                                    isActivated = false
                                }
                            }
                        }
                        false -> {
                            isActivated = true
                            selectedPosition = playNextPosition()
                        }
                    }
                }
            }.lparams(0, 0) {
                topToBottom = R.id.screen_widget_id_11
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                matchConstraintPercentWidth = 0.5f
                dimensionRatio = "w,1:1"
                topMargin = dip(10)
            }

            playingTimeView = nonSpacingTextView("00:00:00") {
                id = R.id.screen_widget_id_13
                typeface = Font.boldByLanguage()
                textSize = 40f
                textColorResource = R.color.x_ffffff
                letterSpacing = -0.01f
                setLineSpacing(-21.1f, 0f)
                lines = 1
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                topToBottom = R.id.screen_widget_id_12
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                topMargin = dip(20)
            }

            textView(R.string.main_screen_label_04) {
                id = R.id.screen_widget_id_15
                textSize = 12f
                textColorResource = R.color.x_ffffff
                setLineSpacing(-4.6f, 0f)
                gravity = Gravity.CENTER
                padding = dip(2)
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToTop = R.id.screen_widget_id_08
                bottomMargin = dip(15)
            }

            radioGroup {
                id = R.id.screen_widget_id_08
                orientation = LinearLayout.HORIZONTAL
                // 명상
                radioButton {
                    id = R.id.screen_widget_id_05
                    buttonDrawableResource = R.drawable.selector_check_music
                    onDebounceClick { currentServiceCategory = ServiceCategory.NORMAL }
                }.lparams(0, matchParent) {
                    weight = 1f
                }
                // 단잠
                radioButton {
                    id = R.id.screen_widget_id_06
                    buttonDrawableResource = R.drawable.selector_check_nature
                    onDebounceClick { currentServiceCategory = ServiceCategory.NATURE }
                }.lparams(0, matchParent) {
                    weight = 1f
                    marginStart = dip(10)
                    marginEnd = dip(10)
                }
                // 스캔
                radioButton {
                    id = R.id.screen_widget_id_07
                    buttonDrawableResource = R.drawable.selector_check_beat
                    onDebounceClick { currentServiceCategory = ServiceCategory.BEAT }
                }.lparams(0, matchParent) {
                    weight = 1f
                }
            }.lparams(wrapContent, dip(37.5f)) {
                startToStart = PARENT_ID
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
                bottomMargin = dip(34)
            }
        }.applyRecursively { v ->
            when (v.id) {
                R.id.screen_widget_id_05,
                R.id.screen_widget_id_06,
                R.id.screen_widget_id_07 -> {
                    // Translate589 버튼 누르는 액션에 따라 재생하는 게 다르다.
                    with(v as RadioButton) {
                        //                        buttonDrawable = null
                        isChecked = when (v.id) {
                            R.id.screen_widget_id_05 -> currentServiceCategory == ServiceCategory.NORMAL
                            R.id.screen_widget_id_06 -> currentServiceCategory == ServiceCategory.NATURE
                            else -> currentServiceCategory == ServiceCategory.BEAT
                        }
                    }
                }
            }
        }
    }.view

    private var isMeasureStarted: Boolean = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AudioPlayer.observePlaybackStatus(this@MainScreen) { status ->
            when (status) {
                is AudioPlayer.PlaybackState.Prepare -> {
                    titleTextView?.run {
                        text = status.audioSource.title

                        when(currentServiceCategory) {
                            ServiceCategory.NORMAL -> R.drawable.ic_text_01
                            ServiceCategory.NATURE -> R.drawable.ic_text_02
                            ServiceCategory.BEAT -> R.drawable.ic_text_03
                        }.let { drawableId ->
                            runOnUiThread {
                                compoundDrawablePadding = dip(5)
                                setCompoundDrawablesWithIntrinsicBounds(drawableId,0,0,0)
                            }
                        }
                    }
                }
                is AudioPlayer.PlaybackState.Start -> {
                    updatePlayingTime(status.currentPosition)

                    if (!isMeasureStarted) {
                        startBrain()
                        isMeasureStarted = true
//                    } else if (availableDataCount == 200) {
//                        OmnifitBrain.stopMeasure()
                    }
                }
                is AudioPlayer.PlaybackState.Playing -> updatePlayingTime(status.currentPosition)
                is AudioPlayer.PlaybackState.Pause -> doseNothing()
                is AudioPlayer.PlaybackState.Resume -> doseNothing()
                is AudioPlayer.PlaybackState.Stop -> {
                    // Translate589 여기가 오디오 멈춘 상태인가?
                    updatePlayingTime(0)
                }
                is AudioPlayer.PlaybackState.Complete -> {
                    // 랜덤 재생
                    durationTime += playlist[status.audioSourcePosition].duration.toInt()
                    selectedPosition = playNextPosition()
                }
                is AudioPlayer.PlaybackState.Error -> doseNothing()
                else -> doseNothing()
            }
        }

        requirePermission()
    }

    private fun checkDeviceConnection() {
        if (!OmnifitBrain.isLinked()) {
            openHeadsetSetting(requireContext())
        }
    }

    private fun updatePlayingTime(millis: Int) {
        playingTimeView?.text = (millis + durationTime).millisToFormatString(DTP_HH_MM_SS)
    }

    private fun playMusic(source: AudioPlayer.AudioSource) {
        AudioPlayer.play(
            requireContext(),
            this@MainScreen,
            listOf(source)
        )
    }

    private fun stopMusic() {
        AudioPlayer.stop()
    }

    private tailrec fun playNextPosition(o: Int = selectedPosition): Int {
        val n: Int = Random.nextInt(0, playlist.size)
        return if (o != n) n else playNextPosition(o)
    }

    private var isMeasurementCanceled: Boolean = false
    private var neuroFeedbackElapsedTime: Int = 0
    private var availableDataCount: Int = 0

    private fun isResultsCanBeViewed(): Boolean {
//        return neuroFeedbackElapsedTime >= 2.minutesToSeconds() && availableDataCount >= 60
        return neuroFeedbackElapsedTime >= 2.minutesToSeconds()
    }

    private fun startBrain() {
        OmnifitBrain.measure(
            eyesState = EyesState.OPENED,
            onState = { state ->
                when (state) {
                    is MeasurementState.Start -> {
                        Timber.e("--> 측정시작 : [${state.elapsedTime.secondsToFormatString(DTP_MM_SS)}][${state.electrodeState}]")
                        isMeasurementCanceled = false
                        availableDataCount = 0
                    }
                    is MeasurementState.Measuring -> {
                        Timber.d(
                            "--> 힐링뮤직 측정중[$availableDataCount][${state.elapsedTime.secondsToFormatString(
                                DTP_MM_SS
                            )}][${state.electrodeState}] 결과보기[${isResultsCanBeViewed()}]"
                        )
                        neuroFeedbackElapsedTime = state.elapsedTime
                        state.electroencephalography?.let { electroencephalography ->
                            if (electroencephalography.isAvailable) {
                                availableDataCount++
                            }
                        }
                    }
                    is MeasurementState.Stop -> {
                        Timber.d(
                            "--> 힐링뮤직 측정 종료[$availableDataCount][${state.elapsedTime.secondsToFormatString(
                                DTP_MM_SS
                            )}][${state.electrodeState}]"
                        )
                        stopMusic()

                        isMeasureStarted = false
                        durationTime = 0

                        when {
                            isMeasurementCanceled -> {

                            }
                            isResultsCanBeViewed() -> {
                                ServiceResultHelper.createHealingMusicResult(
                                    elapsedTime = state.elapsedTime,
                                    result = state.result
                                ) { h ->
                                    Single.timer(100L, TimeUnit.MILLISECONDS)
                                        .subscribeOn(Schedulers.io())
                                        .bindUntilEvent(this@MainScreen, Lifecycle.Event.ON_DESTROY)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                            { moveResultScreen(h) },
                                            { e -> Timber.d(e) }
                                        )
                                }
                            }
                        }
                    }
                }
            },
            onError = { e -> Timber.e(e) }
        )
    }

    private fun moveResultScreen(healing: Healing) {
        screenTo(
            HealingResultScreen()
                .withArguments(
                    Pair(
                        ServiceResultHelper.ARGUMENTS_RESULT_HEALING,
                        healing.toJson()
                    )
                )
        )
    }

    override fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    @Suppress("CheckResult")
    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        if (isStartHealing) {
            selectedPosition = playNextPosition()
        }
    }

    override fun onBackPressed(): Boolean {
        if (isQuickOpened()) {
            closeQuick()
            return true
        }
        if (isMeasureStarted) {
            showNotMeasuredDialog()
            return true
        }
        return appFinish()
    }

    private fun showNotMeasuredDialog(
        onAction: (() -> Unit)? = null
    ) {
        MaterialDialog(requireContext())
            .show {
                hideNavigation()
                customView(
                    view = BasicPopComponent<MaterialDialog>(
                        context.getString(R.string.report_screen_040),
                        context.getString(R.string.healing_music_content_screen_060),
                        object : BasicPopComponent.ButtonActionListener {
                            override fun onPositive() {
                                isMeasurementCanceled = true
                                OmnifitBrain.stopMeasure()
                                dismiss()
                                onAction?.invoke()
                            }

                            override fun onNegative() {
                                dismiss()
                            }
                        }
                    )
                        .createView(AnkoContext.create(context, this))
                        .also { v ->
                            (v as? ConstraintLayout)?.layoutParams =
                                FrameLayout.LayoutParams(
                                    dip(295.5f),
                                    FrameLayout.LayoutParams.WRAP_CONTENT
                                )
                        },
                    noVerticalPadding = true
                )
                cornerRadius(15.0f)
                cancelable(false)
                cancelOnTouchOutside(false)
                setOnDismissListener {
                }
            }
    }

    /*
        Translate589 권한체크 로직, Splash 가 들어가면 거기로 변경
     */
    @SuppressLint("CheckResult")
    private fun requirePermission() {
        if (TedRx2Permission.isGranted(
                context,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) onPermissionGranted()
        else {
            MaterialDialog(requireContext())
                .show {
                    hideNavigation()
                    customView(
                        view = PermissionGuideComponent<MaterialDialog>().createView(
                            AnkoContext.create(
                                requireContext(),
                                this
                            )
                        ),
                        noVerticalPadding = true
                    )
                    cancelable(false)
                    cancelOnTouchOutside(false)
                    setOnDismissListener {
                        TedRx2Permission.with(context)
                            .setPermissions(
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                            .request()
                            .subscribeOn(Schedulers.io())
                            .bindUntilEvent(this@MainScreen, Lifecycle.Event.ON_DESTROY)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { r ->
                                    if (r.deniedPermissions != null) onPermissionDenied(r.deniedPermissions)
                                    else onPermissionGranted()
                                },
                                { e -> Timber.e(e) }
                            )
                    }
                }
        }
    }

    private fun onPermissionGranted() {
        ContentHelper.requireBaseData(requireContext()) {
            ContentHelper.initLoad { progress ->
                if (progress == 102) {
                    checkDeviceConnection()
                }
            }
        }
    }

    private fun onPermissionDenied(permissions: List<String>) {
        appFinish()
    }
}