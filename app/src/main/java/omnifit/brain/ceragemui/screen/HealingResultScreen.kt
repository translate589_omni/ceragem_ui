package omnifit.brain.ceragemui.screen

import android.graphics.*
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans
import omnifit.brain.ceragemui.*
import omnifit.brain.ceragemui.algorithm.label
import omnifit.brain.ceragemui.component.DeepHealingTimeComponent
import omnifit.brain.ceragemui.helper.ServiceResultHelper
import omnifit.brain.ceragemui.helper.ServiceResultHelper.ARGUMENTS_START_HEALING
import omnifit.brain.ceragemui.model.Healing
import omnifit.brain.ceragemui.view.NonSpacingTextView
import omnifit.brain.ceragemui.view.nonSpacingTextView
import omnifit.brain.ceragemui.view.onDebounceClick
import omnifit.sdk.common.algorithm.BrainConcentrationIndicator
import omnifit.sdk.common.json.fromJson
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber

class HealingResultScreen : UIScreen() {

    private val result by lazy {
        arguments?.getString(ServiceResultHelper.ARGUMENTS_RESULT_HEALING)?.let {
            Healing().fromJson(it)
        }
    }

    private val comparisonRate: Int by lazy {
        calcComparisonRate().apply {
            Timber.i("힐링뮤직 전후 결과 : $this")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {

            // 뒤로가기
            imageButton(R.drawable.selector_back_dark) {
                id = R.id.screen_widget_id_01
                setPadding(dip(10.0f))
                onDebounceClick {
                    onBackPressed()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                marginStart = dip(18.2f - 10.0f)
                topToTop = PARENT_ID
                topMargin = dip(41.8f - 10.0f)
            }

            // 타이틀
            nonSpacingTextView(R.string.result_screen_010) {
                id = R.id.screen_widget_id_02
                typeface = Font.mediumByLanguage()
                textSize = 16.0f
                textColorResource = R.color.x_232323
                letterSpacing = -0.04f
                lines = 1
                setLineSpacing(21.5f,0f)
                includeFontPadding = false
            }.lparams(wrapContent, wrapContent) {
                startToStart = PARENT_ID
                topToTop = R.id.screen_widget_id_01
                endToEnd = PARENT_ID
                bottomToBottom = R.id.screen_widget_id_01
                horizontalBias = 0.5f
            }

            nestedScrollView {
                id = R.id.screen_widget_id_05
                constraintLayout {
                    id = R.id.screen_widget_id_06

                    val isSuccess = comparisonRate != 0

                    // 힐링사운드로
                    nonSpacingTextView(R.string.graph_brain_concentration_diff_010) {
                        id = R.id.screen_widget_id_15
                        textSize = 19f
                        typeface = Font.mediumByLanguage()
                        textColorResource = R.color.x_232323
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                    }.lparams(wrapContent, wrapContent) {
                        topToTop = PARENT_ID
                        startToStart = PARENT_ID
                        endToEnd = PARENT_ID
                        topMargin = dip(37.5f)
                    }

                    // 두뇌가 18%
                    nonSpacingTextView {
                        id = R.id.screen_widget_id_17
                        textSize = 19f
                        typeface = Font.mediumByLanguage()
                        textColorResource = R.color.x_232323
                        includeFontPadding = false
                        gravity = Gravity.CENTER

                        getString(R.string.graph_brain_concentration_diff_020, "$comparisonRate%").let { msg ->
                            text = msg

                            val shaderWidth = paint.measureText(msg)
                            paint.shader = LinearGradient (
                                0f, 0f, shaderWidth, lineHeight.toFloat(),
                                ContextCompat.getColor(requireContext(),R.color.x_6c80ff),
                                ContextCompat.getColor(requireContext(),R.color.x_2ba4f5),
                                Shader.TileMode.REPEAT
                            )
                        }
                        isVisible = isSuccess
                    }.lparams(wrapContent, wrapContent) {
                        topToTop = R.id.screen_widget_id_18
                        bottomToBottom = R.id.screen_widget_id_18
                        startToStart = PARENT_ID
                        endToStart = R.id.screen_widget_id_17
                    }

                    // 더 편안해졌어요
                    nonSpacingTextView(
                        when(isSuccess) {
                            true -> R.string.graph_brain_concentration_diff_030
                            false -> R.string.graph_brain_concentration_diff_040
                        }
                    ) {
                        id = R.id.screen_widget_id_18
                        textSize = 19f
                        typeface = Font.mediumByLanguage()
                        textColorResource = R.color.x_232323
                        includeFontPadding = false
                        gravity = Gravity.CENTER
                    }.lparams(wrapContent, wrapContent) {
                        topToBottom = R.id.screen_widget_id_15
                        endToEnd = PARENT_ID
                        topMargin = dip(10.5f)
                        when(isSuccess) {
                            true -> {
                                horizontalChainStyle = ConstraintLayout.LayoutParams.CHAIN_PACKED
                                startToEnd = R.id.screen_widget_id_17
                                marginStart = dip(6.8f)
                            }
                            false -> {
                                startToStart = PARENT_ID
                            }
                        }
                    }

                    // 집중력 변화 그래프
                    cardView {
                        id = R.id.screen_widget_id_07
                        radius = dip(10.0f).toFloat()
                        cardElevation = dip(5.0f).toFloat()
                        backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.shape_r10_rect_result)

                        constraintLayout {
                            id = R.id.screen_widget_id_08
                            //backgroundDrawable = ContextCompat.getDrawable(context,R.drawable.shape_r10_rect_result)

                            nonSpacingTextView(R.string.result_screen_020) {
                                id = R.id.screen_widget_id_09
                                typeface = Font.boldByLanguage()
                                textSize = 14.5f
                                textColorResource = R.color.x_ffffff
                                letterSpacing = -0.02f
                                lines = 1
                                setLineSpacing(42.8f,0f)
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = PARENT_ID
                                marginStart = dip(17.5f)
                                topToTop = PARENT_ID
                                topMargin = dip(24.8f)
                            }

                            view {
                                backgroundColorResource = R.color.x_4ab6ff
                            }.lparams(dip(15.3f),dip(3.8f)) {
                                topToTop = R.id.screen_widget_id_16
                                bottomToBottom = R.id.screen_widget_id_16
                                endToStart = R.id.screen_widget_id_16
                                marginEnd = dip(6.8f)
                            }

                            nonSpacingTextView(R.string.result_screen_030) {
                                id = R.id.screen_widget_id_16
                                typeface = Font.boldByLanguage()
                                textSize = 11f
                                textColorResource = R.color.x_ffffff
                                letterSpacing = -0.05f
                                lines = 1
                                setLineSpacing(46.3f,0f)
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                topToTop = R.id.screen_widget_id_09
                                bottomToBottom = R.id.screen_widget_id_09
                                endToEnd = PARENT_ID
                                marginEnd = dip(19.3f)
                            }

                            // 변화도 그래프
                            lineChart {
                                id = R.id.screen_widget_id_10
                                backgroundResource = R.drawable.ic_graph_line

                                setViewPortOffsets(0.0f, 0.0f, 0.0f, 0.0f)
                                setDrawGridBackground(false)
                                setPinchZoom(false)
                                setScaleEnabled(false)
                                description.isEnabled = false
                                legend.isEnabled = false

                                setTouchEnabled(true)
                                isDragXEnabled = true
                                isDragYEnabled = false

                                xAxis.setDrawAxisLine(false)
                                xAxis.setDrawLabels(false)
                                xAxis.setDrawGridLines(false)

                                axisLeft.setDrawAxisLine(false)
                                axisLeft.setDrawLabels(false)
                                axisLeft.setDrawGridLines(false)
                                axisLeft.labelCount = 15
                                axisLeft.axisMaximum = 10.0f
                                axisLeft.axisMinimum = -4.0f

                                axisRight.isEnabled = false
                            }.lparams(wrapContent, wrapContent) {
                                endToEnd = PARENT_ID
                                marginEnd = dip(24.8f)
                                bottomToBottom = PARENT_ID
                                bottomMargin = dip(25.8f)
                            }

                            nonSpacingTextView {
                                id = R.id.screen_widget_id_11
                                typeface = Font.regularByLanguage()
                                textSize = 10.6f
                                textColorResource = R.color.x_ffffff_op50
                                letterSpacing = -0.05f
                                lines = 1
                                setLineSpacing(-0.05f, 1.4f)
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                topToTop = R.id.screen_widget_id_10
                                topMargin = dip(3.4f)
                                endToStart = R.id.screen_widget_id_10
                                marginEnd = dip(10.5f)
                            }

                            nonSpacingTextView {
                                id = R.id.screen_widget_id_12
                                typeface = Font.regularByLanguage()
                                textSize = 10.6f
                                textColorResource = R.color.x_ffffff_op50
                                letterSpacing = -0.05f
                                lines = 1
                                setLineSpacing(-0.05f, 1.4f)
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_widget_id_11
                                topToTop = R.id.screen_widget_id_10
                                topMargin = dip(66.0f)
                                endToEnd = R.id.screen_widget_id_11
                            }

                            nonSpacingTextView(R.string.result_screen_040) {
                                id = R.id.screen_widget_id_13
                                typeface = Font.regularByLanguage()
                                textSize = 10.6f
                                textColorResource = R.color.x_ffffff_op50
                                letterSpacing = -0.05f
                                setLineSpacing(-0.05f, 1.4f)
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_widget_id_11
                                endToEnd = R.id.screen_widget_id_11
                                bottomToBottom = R.id.screen_widget_id_10
                                bottomMargin = dip(3.4f)
                            }
                        }.lparams(matchParent, matchParent)
                    }.lparams(0, dip(197.5f)) {
                        topMargin = dip(21f)
                        marginStart = dip(19)
                        marginEnd = dip(19)
                        topToBottom = R.id.screen_widget_id_18
                        startToStart = PARENT_ID
                        endToEnd = PARENT_ID
                    }

//                    // 집중시간 그래프
                    DeepHealingTimeComponent<HealingResultScreen>(result!!)
                        .createView(AnkoContext.create(context, this@HealingResultScreen))
                        .also { v ->
                            (v as? CardView)?.let { cv ->
                                cv.id = R.id.screen_widget_id_14
                                cv.layoutParams = ConstraintLayout.LayoutParams(0, dip(167.0f)).apply {
                                    startToStart = R.id.screen_widget_id_07
                                    topToBottom = R.id.screen_widget_id_07
                                    topMargin = dip(19.5f)
                                    endToEnd = R.id.screen_widget_id_07
                                }
                            }
                            addView(v)
                        }

                    imageButton(R.drawable.selector_button_change) {
                        onDebounceClick { onReplayButtonPressed() }
                    }.lparams(0,dip(73f)) {
                        topMargin = dip(14)
                        startToStart = PARENT_ID
                        endToEnd = PARENT_ID
                        topToBottom = R.id.screen_widget_id_14
                        bottomToBottom = PARENT_ID
                        verticalBias = 1.0f
                    }
                }.lparams(matchParent, wrapContent)
            }.lparams(matchConstraint, matchConstraint) {
                topMargin = dip(20)
                startToStart = PARENT_ID
                topToBottom = R.id.screen_widget_id_02
                endToEnd = PARENT_ID
                bottomToBottom = PARENT_ID
            }
        }
    }.view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doAsync {
            result?.let { _result ->
                uiThread {
                    // 변화 그래프
                    with(view.find<LineChart>(R.id.screen_widget_id_10)) {
                        _result.indicatorValues.let { vs ->
                            doubleArrayOf().fromJson(vs).apply {
                                setScaleMinima(size.toFloat().div(45.0f), 0.0f)
                            }
                        }.mapIndexed { idx, v ->
                            Entry(idx.toFloat(), if (v >= 0) v.toFloat() else v.times(2.0).toFloat())
                        }.let { entries ->
                            LineDataSet(entries, null).apply {
                                mode = LineDataSet.Mode.CUBIC_BEZIER
                                cubicIntensity = 0.2f
                                setDrawCircles(false)
                                setDrawHighlightIndicators(false)
                                setDrawValues(false)
                                color = ContextCompat.getColor(context, R.color.x_ffffff)
                            }
                        }.let { dataSet ->
                            LineData(dataSet)
                        }.let { _data ->
                            data = _data
                            this.invalidate()
                        }
                    }
                    // X axis Maximum
                    view.find<NonSpacingTextView>(R.id.screen_widget_id_11).text = BrainConcentrationIndicator.HIGH.label()
                    // X axis Minimum
                    view.find<NonSpacingTextView>(R.id.screen_widget_id_12).text = BrainConcentrationIndicator.LOW.label()
                }
            }
        }
    }

    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        super.onScreenTransitionEnd(screen, isEnter)
    }

    private fun onReplayButtonPressed() {
        screenTo(
            MainScreen().withArguments(ARGUMENTS_START_HEALING to true)
        )
    }

    private fun calcComparisonRate(): Int {
        val checkCount = 30

        return result!!.indicatorValues.run {
            doubleArrayOf().fromJson(this).toList().ignoreNegative()
        }.takeUnless {
            it.size < (checkCount * 2)
        }?.let { list ->
            //val firstCheckValue = list.subList(0,checkCount).average()
            val firstCheckValue = 1f
            val lastCheckValue = list.subList(list.size-checkCount,list.size).average()

            Timber.i("힐링뮤직 전후 처리 : $firstCheckValue / $lastCheckValue")

            when(lastCheckValue > firstCheckValue) {
                true -> ((lastCheckValue / firstCheckValue -1) * 100).toInt()
                false -> 0
            }
        } ?: 0
    }

    // 힐링 사운드 다시 듣기는 다른 메소드로 제공
    override fun onBackPressed(): Boolean {
        screenTo(MainScreen())
        return true
    }
}