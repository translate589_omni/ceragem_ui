package omnifit.brain.ceragemui.view

import android.view.ViewManager
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple
import net.cachapa.expandablelayout.ExpandableLayout
import org.jetbrains.anko.custom.ankoView

inline fun ViewManager.shapeRipple(theme: Int = 0, init: ShapeRipple.() -> Unit): ShapeRipple {
    return ankoView({ ShapeRipple(it) }, theme, init)
}

inline fun ViewManager.expandableLayout(theme: Int = 0, init: ExpandableLayout.() -> Unit): ExpandableLayout {
    return ankoView({ ExpandableLayout(it) }, theme, init)
}
